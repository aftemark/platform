<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Ticket extends Model
{
    use Eloquence;

    protected $fillable = ['event_id', 'category', 'cost_price', 'sell_price', 'dealer_commission', 'agent_commission', 'booking_days', 'booking_hours', 'description'];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function seats()
    {
        return $this->hasMany('App\TicketSeat');
    }
}