<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class TicketSeat extends Model
{
    use SoftDeletes;
    use Eloquence;

    protected $dates = ['deleted_at'];

    public function seat()
    {
        return $this->belongsTo('App\Seat');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
}
