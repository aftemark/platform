<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    public function imageable()
    {
        return $this->morphTo();
    }
}
