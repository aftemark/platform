<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function country ()
    {
        return $this->belongsTo('App\Country');
    }

    public function object() {
        return $this->hasMany('App\Object', 'city_id', 'id');
    }
}