<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    public function seats()
    {
        return $this->hasMany('App\Seat');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function texts()
    {
        return $this->hasMany('App\HallText');
    }
}
