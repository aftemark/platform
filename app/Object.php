<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Object extends Model
{
    use Eloquence;

    protected $searchableColumns = ['name', 'country.name', 'city.name', 'address', 'description'];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function files()
    {
        return $this->morphMany('App\UserFile', 'imageable');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function halls()
    {
        return $this->hasMany('App\Hall');
    }
}