<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    public function object()
    {
        return $this->belongsTo('App\User');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($user) {
            $user->object()->first()->delete();
        });
    }
}
