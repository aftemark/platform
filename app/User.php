<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Sofa\Eloquence\Eloquence;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
     use EntrustUserTrait, Eloquence {
         Eloquence::save insteadof EntrustUserTrait;
         EntrustUserTrait::save insteadof Eloquence;
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $searchableColumns = ['name', 'created_at', 'balance', 'percent', 'days', 'phone', 'email', 'status'];

    public function objects()
    {
        return $this->hasMany('App\Object');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function main_role()
    {
        return $this->belongsTo('App\Role', 'main_role_id', 'id');
    }
}