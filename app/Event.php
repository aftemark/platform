<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Event extends Model
{
    use Eloquence;

    protected $searchableColumns = ['name', 'date', 'object.name', 'description'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function object()
    {
        return $this->belongsTo('App\Object', 'object_id', 'id');
    }

    public function hall()
    {
        return $this->belongsTo('App\Hall');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function files()
    {
        return $this->morphMany('App\UserFile', 'imageable');
    }
}
