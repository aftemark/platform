<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Mailing extends Model
{
    use Eloquence;

    protected $fillable = ['receiver', 'title', 'body'];
}