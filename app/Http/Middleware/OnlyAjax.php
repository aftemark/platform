<?php
namespace App\Http\Middleware;

use Closure;
use Auth;

class OnlyAjax
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest())
            return redirect()->guest('/');

        if (!$request->ajax())
            return response('Forbidden.', 403);


        return $next($request);
    }
}