<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\TicketSeat;
use App\Ticket;
use App\Event;
use App\Transaction;

class TicketSeatController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }

    public function index(Request $request)
    {
        $fields = ['ticket-event-name', 'ticket-category', 'ticket-sell_price', 'seat-row', 'seat-seat'];
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        if ($request->route()->getName() == 'occupied') {
            $transactions = Auth::user()->transactions()->pluck('id');
            $tickets = TicketSeat::whereIn('transaction_id', $transactions);
        } else {
            $ticketTemplates = [];
            foreach (Event::all() as $event) {
                if (Carbon::parse($event->date)->isFuture())
                    foreach ($event->tickets as $eventTicket)
                        $ticketTemplates[] = $eventTicket->id;
            }
            if ($request->has('event_id')) {
                $this->validate($request, [
                    'event_id' => 'exists:events,id'
                ]);

                $tickets = TicketSeat::select('ticket_seats.*')->leftJoin('transactions as tr', 'tr.id', '=', 'ticket_seats.transaction_id')->leftJoin('tickets as ti', 'ti.id', '=', 'ticket_seats.ticket_id')->where('ti.event_id', $request->event_id)->whereIn('ticket_seats.ticket_id',
                    Ticket::whereIn('event_id', Event::where('date', '>', Carbon::now())->pluck('id'))->pluck('id')
                )->where(function ($query) {
                    $query->whereNull('ticket_seats.transaction_id')->orWhere('tr.booked_till', '<', Carbon::now());
                });
            } else
                $tickets = TicketSeat::select('ticket_seats.*')->leftJoin('transactions as tr', 'tr.id', '=', 'ticket_seats.transaction_id')->whereIn('ticket_seats.ticket_id',
                    Ticket::whereIn('event_id', Event::where('date', '>', Carbon::now())->pluck('id'))->pluck('id')
                )->where(function ($query) {
                    $query->whereNull('ticket_seats.transaction_id')->orWhere('tr.booked_till', '<', Carbon::now());
                });
        }

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = str_replace('-', '.', $field);
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $tickets->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            if ($request->sort == 'ticket-event-name')
                $tickets->select('ticket_seats.*')->leftJoin('tickets as ti', 'ti.id', '=', 'ticket_seats.ticket_id')->leftJoin('events as ev', 'ev.id', '=', 'ti.event_id')->orderBy('ev.name', $request->order);
            else if ($request->sort == 'ticket-category')
                $tickets->select('ticket_seats.*')->leftJoin('tickets as ti', 'ti.id', '=', 'ticket_seats.ticket_id')->orderBy('ti.category', $request->order);
            else if ($request->sort == 'ticket-sell_price')
                $tickets->select('ticket_seats.*')->leftJoin('tickets as ti', 'ti.id', '=', 'ticket_seats.ticket_id')->orderBy('ti.sell_price', $request->order);
            else if ($request->sort == 'seat-row')
                $tickets->select('ticket_seats.*')->leftJoin('seats as se', 'se.id', '=', 'ticket_seats.seat_id')->orderBy('se.row', $request->order);
            else if ($request->sort == 'seat-seat')
                $tickets->select('ticket_seats.*')->leftJoin('seats as se', 'se.id', '=', 'ticket_seats.seat_id')->orderBy('se.seat', $request->order);
            else
                $tickets->orderBy($request->sort, $request->order);
        else
            $tickets->orderBy('updated_at', 'desc');

        $tickets = $tickets->paginate($this->num);

        $ticketsDates = [];
        foreach ($tickets as $ticket) {
            $ticketTemplate = $ticket->ticket;
            if (!isset($ticketsDates[$ticketTemplate->id]))
                $ticketsDates[$ticketTemplate->id] = Carbon::parse($ticketTemplate->event->date)->subDays(1)->isFuture();
            if ($ticketsDates[$ticketTemplate->id])
                $ticket->allow_booking = true;
        }

        return view('ticket.index', array('tickets' => $tickets));
    }

    public function view($id)
    {
        return view('ticket.view', [
            'ticket' => TicketSeat::findOrFail($id)
        ]);
    }

    public function preOccupy(Request $request)
    {
        if ($request->has('tickets')) {
            $this->validate($request, [
                'occupy_type' => 'required|in:purchased,booked',
                'tickets.*' => 'required|exists:ticket_seats,id'
            ]);

            $ticketTemplates = [];
            foreach (TicketSeat::whereIn('id', array_values($request->tickets))->get() as $ticket) {
                if (!isset($ticketTemplates[$ticket->ticket_id]))
                    $ticketTemplates[$ticket->ticket_id] = [
                        'name' => $ticket->ticket->event->name . ' - ' . $ticket->ticket->category,
                        'tickets' => []
                    ];
                $ticketTemplates[$ticket->ticket_id]['tickets'][] = $ticket;
            }

            return view('ticket.purchase_index', [
                'ticketTemplates' => $ticketTemplates,
                'occupy_type' => $request->occupy_type
            ]);
        } else
            return 'Не выбраны билеты.';
    }

    public function occupy(Request $request)
    {
        ini_set("log_errors", 1);
        ini_set("error_log", "log.log");
        $this->validate($request, [
            'occupy_type' => 'required|in:purchased,booked',
            'tickets.*' => 'exists:ticket_seats,id'
        ]);

        if ($request->has('tickets')) {
            $events = [];
            foreach ($request->tickets as $ticketID) {
                $ticket = TicketSeat::find($ticketID);
                if ($ticket->transaction_id && ($ticket->transaction->price || Carbon::parse($ticket->transaction->booked_till)->isFuture()))
                    return response($ticket->id . ' билет уже куплен/забронирован.', 422);
                $ticketTemplate = $ticket->ticket;
                if (!Carbon::parse($ticketTemplate->event->date)->isFuture())
                    return response('Событие уже прошло.', 422);
                //if ($request->occupy_type == 'booked' && (Carbon::parse($ticketTemplate->created_at)->addDays($ticketTemplate->booking_days)->addHours($ticketTemplate->booking_hours)->isPast() || Carbon::parse($ticketTemplate->event->date)->subDays(1)->isPast()))
                //return response('Время бронирования билета ' . $ticket->id . ' вышло.', 422);
                if ($request->occupy_type == 'booked' && Carbon::parse($ticketTemplate->event->date)->subDays(1)->isPast())
                    return response('Время бронирования билета ' . $ticket->id . ' вышло.', 422);
                if (!isset($events[$ticket->ticket_id])) {
                    $transaction = new Transaction;
                    $transaction->user_id = Auth::user()->id;
                    $transaction->ticket_id = $ticket->ticket_id;
                    if ($request->occupy_type == 'purchased')
                        $transaction->price = $ticket->ticket->sell_price;
                    if ($request->occupy_type == 'booked' && Carbon::parse($ticketTemplate->event->date)->subDays(1)->isFuture()) {
                        $bookDate = Carbon::now()->addDays($ticketTemplate->booking_days)->addHours($ticketTemplate->booking_hours);
                        $lastDate = Carbon::parse($ticketTemplate->event->date)->subDays(1);
                        error_log($transaction->ticket_id . ': ' . json_encode($bookDate) . ' last: ' . json_encode($lastDate));
                        $transaction->booked_till = $lastDate->diffInSeconds($bookDate, false) ? $lastDate : $bookDate;
                    }
                    $transaction->save();
                    $events[$transaction->ticket_id] = $transaction;
                } else if ($request->occupy_type == 'purchased') {
                    $events[$ticket->ticket_id]->price += $ticket->ticket->sell_price;
                    $events[$ticket->ticket_id]->save();
                }

                $ticket->transaction_id = $events[$ticket->ticket_id]->id;
                $ticket->save();
            }
        }

        return redirect()->route('occupied');
    }

    public function free($id)
    {
        $ticket = TicketSeat::findOrFail($id);

        if ($ticket->status == 'purchased')
            $ticket->delete();
        else if ($ticket->status == 'booked') {
            $transaction = $ticket->transaction;
            $ticket->transaction_id = NULL;
            $ticket->save();

            if (!$transaction->seats->count())
                $transaction->delete();
        }

        return redirect()->back();
    }
}
