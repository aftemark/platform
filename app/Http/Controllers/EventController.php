<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use App\Event;
use App\Object;
use App\Hall;
use App\UserFile;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Validator;
use Auth;
use Session;

class EventController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }

    public function index(Request $request)
    {
        /*if (Auth::user()->hasRole('stickler') || Auth::user()->hasRole('agent') || Auth::user()->hasRole('customer'))
            if ($request->has('num'))
                $events = Event::paginate($request->num);
            else
                $events = Event::paginate(10);
        else
            if ($request->has('num'))
                $events = Event::where('user_id', '=', Auth::user()->id)->paginate($request->num);
            else
                $events = Event::where('user_id', '=', Auth::user()->id)->paginate(10);

        foreach ($events as $event) {
            $event->photos = array();
            foreach ($event->files as $file)
                if (Storage::disk('public')->exists($file->filename))
                    $event->photos = $event->photos + array($file->original_filename => '/images/' . $file->filename);
        }*/
        $fields = ['name', 'date', 'object_name', 'description'];
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);
        
        if (Auth::user()->main_role->name == 'seller')
            $events = Auth::user()->events();
        else {
            if ($request->has('object_id')) {
                $this->validate($request, [
                    'object_id' => 'exists:objects,id'
                ]);
                $events = Event::where('object_id', $request->object_id);
            } else
                $events = Event::select('*');
        }

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = str_replace('_', '.', $field);
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $events->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            if ($request->sort == 'object_name')
                $events->select('events.*')->leftJoin('objects as obj', 'obj.id', '=', 'events.object_id')->orderBy('obj.name', $request->order);
            else
                $events->orderBy($request->sort, $request->order);

        $events = $events->paginate($this->num);

        foreach ($events as $event) {
            $event->photos = array();
            foreach ($event->files as $file)
                if (Storage::disk('public')->exists($file->filename))
                    $event->photos = $event->photos + array($file->original_filename => '/images/' . $file->filename);
        }

        return view('event.index', array('events' => $events));
    }

    public function view($item)
    {
        $role = Auth::user()->main_role->name;
        $view = 'view';
        if (in_array($role, ['stickler', 'seller'])) {
            if ($role == 'stickler') {
                $event = $item != 'new' ? Event::findOrFail($item) : new Object;
                $parameters = ['objects' => Object::pluck('name', 'id')];
            } else {
                $event = $item != 'new' ? Auth::user()->events()->findOrFail($item) : new Object;
                $parameters = ['objects' => Auth::user()->objects()->pluck('name', 'id')];
            }
            $view = 'edit';
            $parameters += ['allowSave' => HelperTrait::checkEntity($event, ['tickets'], true)];
        } else {
            if ($item != 'new')
                $event = Event::findOrFail($item);
            else
                return response(404);
            $parameters = [];
        }

        $event->links = array();
        foreach ($event->files as $file)
            if (Storage::disk('public')->exists($file->filename))
                $event->links = $event->links + array($file->original_filename => '/images/' . $file->filename);

        return view('event.' . $view, $parameters + [
            'event' => $event
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'date' => 'date',
            'object' => 'required|exists:objects,id',
            'hall' => 'required|exists:halls,id',
            'description' => 'max:9999'
        ]);
        if ($request->has('id')) {
            if (Auth::user()->main_role->name == 'stickler')
                $event = Event::find($request->id);
            else
                $event = Auth::user()->events()->find($request->id);

            if (!HelperTrait::checkEntity($event, ['tickets'], true))
                return response('Сущность не может быть редактирована.', 422);
        } else
            $event = new Event;

        if (!Auth::user()->objects()->where('id', $request->object)->exists())
            return response('Неверный объект.', 422);
        if (Hall::find($request->hall)->object_id != $request->object)
            return response('Неверный зал.', 422);

        $event->name = $request->name;
        $event->date = $request->date;
        $event->object_id = $request->object;
        $event->hall_id = $request->hall;
        $event->description = $request->description;
        $event->user_id = Auth::user()->id;

        $event->save();

        if ($event->remove = 'remove')
            foreach ($event->files as $file) {
                File::delete(storage_path('app/public') . '/' . $file->filename);
                $file->delete();
            }

        if($request->hasFile('files')) {
            $files = $request->files;

            foreach($files as $filebag) {
                foreach ($filebag as $file) {
                    $rules = array('file' => 'mimes:jpeg,bmp,png');
                    $validator = Validator::make(array('file' => $file), $rules);
                    if ($validator->passes()) {
                        $extension = $file->getClientOriginalExtension();
                        $filename = $file->getFilename() . '.' . $extension;
                        Storage::disk('public')->put($filename, File::get($file));

                        if (Storage::disk('public')->exists($filename)) {
                            $userFile = new UserFile;
                            $userFile->original_filename = $file->getClientOriginalName();
                            $userFile->filename = $filename;
                            $userFile->mime_type = $file->getClientMimeType();
                            $userFile->size = Storage::disk('public')->size($filename);

                            $event->files()->save($userFile);
                        }
                    }
                }
            }
        }
        return redirect()->back()->with('message', "Мероприятие успешно сохранено!");
    }

    public function delete($id)
    {
        $event = Auth::user()->main_role->name == 'stickler' ? Event::findOrfail($id) : Auth::user()->events()->findOrFail($id);
        HelperTrait::checkEntity($event, ['tickets']);

        foreach ($event->files as $file) {
            File::delete(storage_path('app/public') . '/' . $file->filename);
            $file->delete();
        }

        $event->delete();

        return redirect()->back();
    }
}
