<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\User;
use App\Role;

class DealerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }

    public function index(Request $request)
    {
        $fields = ['name', 'created_at', 'balance', 'percent', 'days', 'phone', 'email'];
        $this->validate($request, [
            'sort' => 'in:status,' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        $dealers = Auth::user()->users();

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = $field;
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $dealers->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            $dealers->orderBy($request->sort, $request->order);

        $dealers = $dealers->paginate($this->num);

        return view('dealer.index', [
            'dealers' => $dealers
        ]);
    }
    public function store(Request $request)
    {
        $validate = [
            'name' => 'required|max:255',
            'phone' => 'min:6|max:255',
            'status' => 'in:1,0',
            'percent' => 'integer|min:0',
            'balance' => 'numeric|min:0'
        ];
        if ($request->has('id')) {
            $validate = $validate + [
                    'id' => 'exists:users,id',
                    'password' => 'min:6'
                ];

            $user = Auth::user()->users()->findOrFail($request->id);
            $user->name = $request->name;
            $user->phone = $request->phone;

            $user->detachRoles($user->roles);

            if ($request->has('password'))
                $user->password = bcrypt($request->password);
        } else {
            $validate = $validate + [
                    'password' => 'required|min:6',
                    'email' => 'email|max:255|unique:users'
                ];

            $user = new User;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->user_id = Auth::user()->id;
        }
        $this->validate($request, $validate);

        $user->status = $request->status;
        $user->percent = $request->percent;
        $user->balance = $request->balance;

        $user->save();
        $user->attachRole(Role::where('name', 'dealer')->first());

        return redirect()->route('dealers.show', ['id' => $user->id])->with('message', 'Дилер успешно сохранен.');
    }
    
    public function show($item)
    {
        return view('dealer.view', [
            'dealer' => $item != 'new' ? Auth::user()->users()->findOrFail($item) : new User
        ]);
    }
    
    public function delete($id)
    {
        $dealer =  Auth::user()->users()->findOrFail($id);
        HelperTrait::checkEntity($dealer, ['objects', 'events', 'users', 'transactions', 'tickets']);
        $dealer->delete();

        return redirect()->back();
    }
}
