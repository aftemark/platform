<?php

namespace App\Http\Controllers;
use App\Country;
use App\City;
use Illuminate\Http\Request;

use App\Http\Requests;

class CountryController extends Controller
{
    public function getCountries ()
    {
        return Country::all();
    }

    public function getCities ($id)
    {
        return Country::find($id)->cities;
    }
}