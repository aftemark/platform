<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Hall;
use App\HallText;
use App\Seat;
use Auth;

class HallController extends Controller
{
    public function view(Request $request, $item)
    {
        if ($item != 'new') {
            $hall = Hall::findOrFail($item);
            $objectID = $hall->object_id;
        } else {
            $this->validate($request, [
                'object' => 'required|exists:objects,id'
            ]);

            $hall = new Hall;
            $objectID = $request->object;
        }

        $allowSave = true;
        foreach ($hall->events as $event)
            if (count($event->tickets)) {
                $allowSave = false;
                break;
            }

        return view('hall.view', [
            'hall' => $hall,
            'objectID' => $objectID,
            'allowSave' => $allowSave
        ]);
    }

    public function store(Request $request)
    {
        $validate = [
            'name' => 'required|string',
            'seat.left.*' => 'numeric|between:0,690',
            'seat.top.*' => 'numeric|between:0,690',
            'seat.row.*' => 'numeric|integer',
            'seat.number.*' => 'numeric|integer',
            'text.left.*' => 'numeric|between:0,690',
            'text.top.*' => 'numeric|between:0,690',
            'text.scaleX.*' => 'numeric',
            'text.scaleY.*' => 'numeric',
            'text.text.*' => 'string',
        ];
        if ($request->has('id')) {
            $this->validate($request, $validate + ['id' => 'exists:halls,id']);
            $hall = Hall::find($request->id);
            if (Auth::user()->main_role->name != 'stickler' && !Auth::user()->objects()->find($hall->object_id)->exists())
                return response('Неверный объект.', 403);

            foreach ($hall->events as $event)
                if ($event->tickets->count())
                    return response('У зала есть билеты.', 422);
            $hall->seats()->delete();
            $hall->texts()->delete();
        } else {
            $this->validate($request, $validate + ['object' => 'required|exists:objects,id']);
            $hall = new Hall;
            $hall->object_id = $request->object;
            if (Auth::user()->main_role->name != 'stickler' && !Auth::user()->objects()->find($hall->object_id)->exists())
                return response('Неверный объект.', 403);
        }

        $hall->name = $request->name;
        $hall->save();

        $seats = [];
        if (!empty($request['seat']['number'])) {
            foreach ($request['seat']['number'] as $seatCount => $seatNum) {
                if ($request->has('seat.left.' . $seatCount) && $request->has('seat.top.' . $seatCount) && $request->has('seat.number.' . $seatCount) && !isset($seats[$request['seat']['row'][$seatCount]][$seatNum])) {
                    $seat = new Seat;
                    $seat->hall_id = $hall->id;
                    $seat->left = $request['seat']['left'][$seatCount];
                    $seat->top = $request['seat']['top'][$seatCount];
                    $seat->row = $request['seat']['row'][$seatCount];
                    $seat->seat = $seatNum;
                    $seat->save();

                    $seats[$seat->row][] = $seatNum;
                }
            }
        }

        if (!empty($request['text']['text'])) {
            foreach ($request['text']['text'] as $textCount => $body) {
                if ($request->has('text.left.' . $textCount) && $request->has('text.top.' . $textCount) && $request->has('text.scaleX.' . $textCount) && $request->has('text.scaleY.' . $textCount)) {
                    $text = new HallText;
                    $text->hall_id = $hall->id;
                    $text->body = $body;
                    $text->left = $request['text']['left'][$textCount];
                    $text->top = $request['text']['top'][$textCount];
                    $text->scale_x = $request['text']['scaleX'][$textCount];
                    $text->scale_y = $request['text']['scaleY'][$textCount];
                    $text->save();
                }
            }
        }
        
        return response()->json([
            'url' => url('/object/' . $hall->object_id)
        ]);
    }

    public function data(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:halls,id'
        ]);

        $hall = Hall::findOrFail($request->id);

        return response()->json([
            'seats' => $hall->seats,
            'texts' => $hall->texts
        ]);
    }
}
