<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Mailing;
use App\Role;
use Mail;

class MailingController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }

    public function index(Request $request)
    {
        $fields = ['created_at', 'receiver', 'body'];
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        $mailings = Mailing::select('*');

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = str_replace('_', '.', $field);
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $mailings->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            $mailings->orderBy($request->sort, $request->order);

        $mailings = $mailings->paginate($this->num);

        return view('mailing.index', [
            'mailings' => $mailings
        ]);
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'receiver' => 'required|in:Дилеры,Агенты,Продавцы,Покупатели',
            'subject' => 'required|max:255',
            'body' => 'required',
        ]);

        $mail = new Mailing;
        $mail->fill($request->all());
        $mail->save();

        if ($mail->receiver == 'Дилеры')
            $roleName = 'dealer';
        else if ($mail->receiver == 'Агенты')
            $roleName = 'agent';
        else if ($mail->receiver == 'Продавцы')
            $roleName = 'seller';
        else
            $roleName = 'customer';
        $emails = Role::where('name', $roleName)->first()->users()->pluck('email');

        $count = 0;
        $emailData = [
            'subject' => $mail->subject,
            'body' => $mail->body
        ];
        foreach ($emails as $email) {
            $emailData['email'] = $email;
            Mail::send('emails.mass', ['emailData' => $emailData], function ($m) use ($emailData) {
                $m->to($emailData['email'])->subject($emailData['subject']);
            });
            $count++;
        }

        return redirect()->route('mailing')->with(['message' => $count . ' сообщений было отправлено.']);
    }
}
