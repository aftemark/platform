<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ticket;
use App\Event;
use App\TicketSeat;
use App\Transaction;

class TicketController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }
    
    public function index(Request $request)
    {
        $fields = ['event-name', 'category', 'sell_price', 'description'];
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        $tickets = Auth::user()->main_role->name == 'stickler' ? Ticket::select('*') : Auth::user()->tickets();

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = str_replace('-', '.', $field);
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $tickets->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            if ($request->sort == 'event-name')
                $tickets->select('tickets.*')->leftJoin('events as ev', 'ev.id', '=', 'tickets.event_id')->orderBy('ev.name', $request->order);
            else
                $tickets->orderBy($request->sort, $request->order);
        else
            $tickets->orderBy('updated_at', 'desc');

        $tickets = $tickets->paginate($this->num);

        return view('ticket.stickler_index', [
            'tickets' => $tickets,
            'transactions' => Transaction::where('price', NULL)->pluck('id')
        ]);
    }

    public function view($item)
    {
        $ticket = $item != 'new' ? Ticket::findOrFail($item) : new Ticket;

        return view('ticket.stickler_view', [
            'ticket' => $ticket,
            'allowSave' => !$ticket->seats()->where('transaction_id', '!=', NULL)->exists(),
            'events' => Auth::user()->main_role->name == 'stickler' ? Event::pluck('name', 'id') : Auth::user()->events()->pluck('name', 'id')
        ]);
    }

    public function store(Request $request)
    {
        $validate = [
            'event_id' => 'required|exists:events,id',
            'category' => 'required',
            'cost_price' => 'required|numeric',
            'sell_price' => 'required|numeric',
            'dealer_commission' => 'required|integer|numeric|between:0,100',
            'agent_commission' => 'required|integer|numeric|between:0,100',
            'description' => 'max:9999',
        ];
        if (!$request->has('no_seats'))
            $validate += ['seats.*' => 'exists:seats,id'];
        else
            $validate += ['custom_seats' => 'required|integer|numeric|min:1'];

        if ($request->has('id')) {
            $this->validate($request, $validate + [
                'id' => 'required|exists:tickets,id'
            ]);
            $ticket = Auth::user()->main_role->name == 'stickler' ? Ticket::find($request->id) : Auth::user()->tickets()->findOrFail($request->id);
            if ($ticket->seats()->where('transaction_id', '!=', NULL)->exists())
                return response('Сущность не может быть редактирована.', 422);
            $ticket->seats()->forceDelete();
        } else {
            $this->validate($request, $validate);
            $ticket = new Ticket;
        }

        if (Auth::user()->main_role->name != 'stickler' && !Auth::user()->events()->find($request->event_id)->exists())
            return response('Неверное мероприятие.', 403);
        $ticket->fill($request->all());
        $ticket->user_id = Auth::user()->id;
        $ticket->allow_seller = $request->has('allow_seller');
        $ticket->allow_dealer = $request->has('allow_dealer');
        $ticket->allow_agent = $request->has('allow_agent');
        if (!$request->has('no_seats'))
            $ticket->custom_seats = NULL;
        else if ($request->has('custom_seats'))
            $ticket->custom_seats = $request->custom_seats;
        $ticket->custom_seats = $request->has('no_seats') ? NULL : $request->custom_seats;
        $ticket->save();

        if ($request->has('no_seats')) {
            for ($i = 1; $i <= $ticket->custom_seats; $i++) {
                $ticketSeat = new TicketSeat;
                $ticketSeat->ticket_id = $ticket->id;
                $ticketSeat->save();
            }
        } else if ($request->has('seats')) {
            foreach ($ticket->event->hall->seats as $seat)
                if (in_array($seat->id, $request->seats)) {
                    $ticketSeat = new TicketSeat;
                    $ticketSeat->seat_id = $seat->id;
                    $ticketSeat->ticket_id = $ticket->id;
                    $ticketSeat->save();
                }
        } else
            return redirect()->back();

        return redirect()->route('stickler.tickets');
    }

    public function hall(Request $request)
    {
        $this->validate($request, [
            'id' => 'exists:tickets,id',
            'event_id' => 'required|exists:events,id'
        ]);

        if ($request->has('id')) {
            $ticket = Ticket::find($request->id);
            $event = $ticket->event_id == $request->event_id ? $ticket->event : Event::find($request->event_id);
            $ticket_id = $ticket->id;
        } else {
            $event = Event::find($request->event_id);
            $ticket_id = NULL;
        }
        $seats = collect($event->hall->seats)->keyBy('id');
        foreach ($event->tickets as $eventTicket) {
            $status = 'occupied';
            if ($ticket_id == $eventTicket->id)
                $status = 'selected';
            foreach ($eventTicket->seats()->withTrashed()->get() as $eventTicketSeat) {
                $seats[$eventTicketSeat->seat_id]['status'] = $status;
            }
        }

        return response()->json([
            'seats' => $seats,
            'texts' => $event->hall->texts
        ]);
    }

    public function delete($id)
    {
        $ticket = Auth::user()->main_role->name == 'stickler' ? Ticket::findOrfail($id) : Auth::user()->tickets()->findOrFail($id);
        if ($ticket->seats()->where('transaction_id', '!=', NULL)->exists())
            return response('Сущность не может быть удалена.', 422);

        $ticket->delete();
    }
}
