<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\User;
use App\Role;

class AgentController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }

    public function index(Request $request)
    {
        $fields = ['name', 'created_at', 'balance', 'percent', 'days', 'phone', 'email'];
        $this->validate($request, [
            'sort' => 'in:status,' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        $agents = Auth::user()->users();

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = $field;
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $agents->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            $agents->orderBy($request->sort, $request->order);

        $agents = $agents->paginate($this->num);

        return view('agent.index', [
            'agents' => $agents
        ]);
    }
    public function store(Request $request)
    {
        $validate = [
            'name' => 'required|max:255',
            'phone' => 'min:6|max:255',
            'status' => 'in:1,0',
            'days' => 'integer|min:0',
            'percent' => 'integer|min:0',
            'balance' => 'numeric|min:0'
        ];
        if ($request->has('id')) {
            $validate = $validate + ['id' => 'exists:users,id'];

            $user = Auth::user()->users()->findOrFail($request->id);
            $user->name = $request->name;
            $user->phone = $request->phone;

            $user->detachRoles($user->roles);

            if ($request->has('password'))
                $user->password = bcrypt($request->password);
        } else {
            $validate = $validate + [
                    'password' => 'required|min:6',
                    'email' => 'email|max:255|unique:users'
                ];

            $user = new User;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->user_id = Auth::user()->id;
        }
        $this->validate($request, $validate);

        $user->status = $request->status;
        $user->percent = $request->percent;
        $user->balance = $request->balance;
        $user->days = $request->days;

        $user->save();
        $user->attachRole(Role::where('name', '=', 'agent')->first());

        return redirect()->back()->with('message', 'Агент успешно сохранен');
    }
    
    public function show($item)
    {
        return view('agent.view', [
            'agent' => $item != 'new' ? Auth::user()->users()->findOrFail($item) : new User
        ]);
    }
    
    public function delete($id)
    {
        $agent = User::findOrFail($id);
        HelperTrait::checkEntity($agent, ['objects', 'events', 'users', 'transactions', 'tickets']);
        $agent->delete();

        return redirect()->back();
    }
}
