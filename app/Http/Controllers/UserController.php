<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Validator;
use Eloquence;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }
    
    public function register()
    {
        if (Auth::check())
            return redirect('/personal');
        else
            return view('auth.register', array('dealers' => Role::where('name', 'dealer')->first()->users()->get()));
    }

    public function postSignUp(Request $request)
    {
        $validate = [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'phone' => 'required|min:6',
            'country' => 'required|exists:countries,id',
            'city' => 'required|exists:cities,id',
            'password' => 'required|min:6|confirmed:password_confirmation',
            'password_confirmation' => 'required|min:6',
            'type' => 'required|in:customer,agent'
        ];

        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails())
            return redirect()
                ->back()
                ->with('type', $request->input('type', 'customer'))
                ->withErrors($validator)
                ->withInput();

        $user = new User;
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->phone = $request['phone'];
        $user->country_id = $request['country'];
        $user->city_id = $request['city'];
        $user->password = bcrypt($request['password']);

        $user->save();

        $user->attachRole($request['type'] == 'customer' ? 1 : 3);

        return redirect()->route('dashboard');
    }

    public function index(Request $request)
    {
        $fields = ['name', 'created_at', 'balance', 'percent', 'days', 'phone', 'email', 'main_role_display_name'];
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        $users = User::select('*');

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields + ['main_role_display_name'] as $field)
            if ($request->has($field)) {
                if ($field != 'main_role_display_name')
                    $search['fields'][] = $field;
                else
                    $search['fields'][] = 'main_role.display_name';
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $users->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            if ($request->sort != 'main_role_display_name')
                $users = $users->orderBy($request->sort, $request->order);
            else
                $users->select('users.*')->leftJoin('roles as ro', 'ro.id', '=', 'users.main_role_id')->orderBy('ro.name', $request->order);

        $users = $users->paginate($this->num);

        return view('user.index', array('users' => $users));
    }

    public function view($item)
    {
        return view('user.view', [
            'user' => $item == 'new' ? new User : User::findOrFail($item),
            'roles' => Role::orderBy('created_at', 'asc')->get()
        ]);
    }

    public function store(Request $request)
    {
        $types = Role::orderBy('created_at', 'asc')->get();
        $typenames = [];
        foreach ($types as $type)
            $typenames[] = $type->name;

        $validate = [
            'days' => 'numeric|integer',
            'percent' => 'numeric|integer|between:0,100',
            'balance' => 'numeric|min:0'
        ];

        if ($request->has('id')) {
            $validate = $validate + [
                'name' => 'required|max:255',
                'phone' => 'numeric|integer|min:0',
                'type' => 'required|in:' . implode(",", $typenames)
            ];
            $this->validate($request, $validate);

            $user = User::findOrFail($request->id);
            $user->name = $request->name;
            $user->phone = $request->phone;

            $user->detachRoles($user->roles);

            if ($request->has('password'))
                $user->password = bcrypt($request->password);
        } else {
            $validate = $validate + [
                'name' => 'required|max:255',
                'phone' => 'numeric|integer|min:0',
                'email' => 'email|max:255|unique:users',
                'type' => 'required|in:' . implode(",", $typenames),
                'password' => 'required|min:6'
            ];
            $this->validate($request, $validate);

            $user = new User;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
        }
        $user->days = $request->days;
        $user->percent = $request->percent;
        $user->balance = $request->balance;

        $role = Role::where('name', '=', $request->type)->first();
        $user->main_role_id = $role->id;

        $user->save();

        $user->attachRole($role);

        return redirect('/user/' . $user->id)->with('message', 'Пользователь успешно сохранен');
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        HelperTrait::checkEntity($user, ['objects', 'events', 'users', 'transactions', 'tickets']);
        $user->delete();

        return redirect()->back();
    }

    public function indexPersonal()
    {
        $user = Auth::user();

        $dealers = [];
        if (isset($user->main_role) && $user->main_role->name == 'agent') {
            $dealer_role = Role::where('name', 'dealer')->first();
            $dealers = $dealer_role->users()->pluck('name', 'id');
        }

        return view('personal', [
            'user' => $user,
            'dealers' => $dealers
        ]);
    }

    public function storePersonal(Request $request)
    {
        $validate = [
            'name' => 'required|min:3|max:255',
            'phone' => 'min:6|max:255',
            'email' => 'required|email',
            'dealer' => 'exists:users,id'
        ];

        if ($request->has('password'))
            $validate = $validate + [
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required|min:6'
                ];

        $this->validate($request, $validate);

        $user = Auth::user();

        if ($user->user_id != $request->dealer)
            $user->status = 0;

        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->user_id = $request->dealer;
        $user->password = bcrypt($request->password);

        $user->save();

        return redirect()->back()->with('message', 'Изменения успешно сохранены');
    }

}