<?php

namespace App\Http\Controllers;

use App\Http\Traits\HelperTrait;
use Illuminate\Http\Request;
use App\Object;
use App\UserFile;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Validator;
use Auth;

class ObjectController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }

    public function index(Request $request)
    {
        $fields = ['name', 'country_name', 'city_name', 'address', 'description'];
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        if (Auth::user()->main_role->name == 'seller')
            $objects = Auth::user()->objects();
        else
            $objects = Object::select('*');

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = str_replace('_', '.', $field);
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $objects->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            if ($request->sort == 'country_name')
               $objects->select('objects.*')->leftJoin('countries as co', 'co.id', '=', 'objects.country_id')->orderBy('co.name', $request->order);
            else if ($request->sort == 'city_name')
                $objects->select('objects.*')->leftJoin('cities as ci', 'ci.id', '=', 'objects.city_id')->orderBy('ci.name', $request->order);
            else
                $objects->orderBy($request->sort, $request->order);

        $objects = $objects->paginate($this->num);

        foreach ($objects as $object) {
            $object->photos = array();
            foreach ($object->files as $file)
                if (Storage::disk('public')->exists($file->filename))
                    $object->photos = $object->photos + array($file->original_filename => '/images/' . $file->filename);
        }

        return view('object.index', [
            'objects' => $objects
        ]);
    }

    public function view($item)
    {
        $role = Auth::user()->main_role->name;
        $view = 'view';
        if (in_array($role, ['stickler', 'seller'])) {
            if ($role == 'stickler')
                $object = $item != 'new' ? Object::findOrFail($item) : new Object;
            else
                $object = $item != 'new' ? Auth::user()->objects()->findOrFail($item) : new Object;
            $view = 'edit';
        } else {
            if ($item != 'new')
                $object = Object::findOrFail($item);
            else
                return response(404);
        }

        $object->links = array();
        foreach ($object->files as $file)
            if (Storage::disk('public')->exists($file->filename))
                $object->links = $object->links + array($file->original_filename => '/images/' . $file->filename);

        return view('object.' . $view, [
            'object' => $object
        ]);
    }

    public function store(Request $request)
    {
        $role = Auth::user()->main_role->name;
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'max:9999',
            'address' => 'min:6|max:255',
            'city' => 'required|exists:cities,id',
            'country' => 'required|exists:countries,id'
        ]);
        if ($request->has('id')) {
            if ($role == 'stickler')
                $object = Object::find($request->id);
            else
                $object = Auth::user()->objects()->findOrFail($request->id);
            $returnBack = false;
        } else {
            $object = new Object;
            $returnBack = true;
        }

        $object->name = $request->name;
        $object->description = $request->description;
        $object->address = $request->address;
        $object->country_id = $request->country;
        $object->city_id = $request->city;
        $object->user_id = Auth::user()->id;

        if (!$object->country->cities()->where('id', $request->city)->exists())
            return response('Неверная страна.', 422);

        $object->save();

        if ($object->remove = 'remove')
            foreach ($object->files as $file) {
                File::delete(storage_path('app/public') . '/' . $file->filename);
                $file->delete();
            }

        if ($request->hasFile('files')) {
            $files = $request->files;

            foreach($files as $filebag) {
                foreach ($filebag as $file) {
                    $rules = array('file' => 'mimes:jpeg,bmp,png');
                    $validator = Validator::make(array('file' => $file), $rules);
                    if ($validator->passes()) {
                        $extension = $file->getClientOriginalExtension();
                        $filename = $file->getFilename() . '.' . $extension;
                        Storage::disk('public')->put($filename, File::get($file));

                        if (Storage::disk('public')->exists($filename)) {
                            $userFile = new UserFile;
                            $userFile->original_filename = $file->getClientOriginalName();
                            $userFile->filename = $filename;
                            $userFile->mime_type = $file->getClientMimeType();
                            $userFile->size = Storage::disk('public')->size($filename);

                            $object->files()->save($userFile);
                        }
                    }
                }
            }
        }

        return $returnBack ? redirect('/object/' . $object->id) : redirect()->route('objects');
    }

    public function delete($id)
    {
        $object = Auth::user()->main_role->name == 'stickler' ? Object::findOrfail($id) : Auth::user()->objects()->findOrFail($id);
        HelperTrait::checkEntity($object, ['events']);

        foreach ($object->files as $file) {
            File::delete(storage_path('app/public') . '/' . $file->filename);
            $file->delete();
        }

        $object->delete();
    }

    /*public function events($id)
    {
        $object = Object::findOrFail($id);
        $events = $object->events();

        foreach ($events as $event) {
            $event->photos = array();
            foreach ($event->files as $file)
                if (Storage::disk('public')->exists($file->filename))
                    $event->photos = $event->photos + array($file->original_filename => '/images/' . $file->filename);
        }

        return view('object.events', array('events' => $events));
    }*/

    public function halls(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:objects,id'
        ]);

        return response()->json(Object::findOrFail($request->id)->halls()->pluck('name', 'id'));
    }
}