<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Transaction;

class TransactionController extends Controller
{
    public function __construct(Request $request)
    {
        $this->num = $request->session()->has('num') ? $request->session()->get('num') : 10;
    }

    public function index(Request $request)
    {
        $fields = ['created_at', 'user-created_at', 'ticket-user-name', 'user-main_role-display_name', 'ticket-event-name', 'ticket-category', 'user-name', 'price'];
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        $transactions = Transaction::select('*');

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = str_replace('-', '.', $field);
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $transactions->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            if ($request->sort == 'user-created_at')
                $transactions->select('transactions.*')->leftJoin('users as us', 'us.id', '=', 'transactions.user_id')->orderBy('us.created_at', $request->order);
            else if ($request->sort == 'ticket-user-name')
                $transactions->select('transactions.*')->leftJoin('tickets as ti', 'ti.id', '=', 'transactions.ticket_id')->leftJoin('users as us', 'us.id', '=', 'ti.event_id')->orderBy('us.name', $request->order);
            else if ($request->sort == 'user-main_role-display_name')
                $transactions->select('transactions.*')->leftJoin('users as us', 'us.id', '=', 'transactions.ticket_id')->leftJoin('roles as ro', 'ro.id', '=', 'us.main_role_id')->orderBy('ro.display_name', $request->order);
            else if ($request->sort == 'ticket-event-name')
                $transactions->select('transactions.*')->leftJoin('tickets as ti', 'ti.id', '=', 'transactions.ticket_id')->leftJoin('events as ev', 'ev.id', '=', 'ti.event_id')->orderBy('ev.name', $request->order);
            else if ($request->sort == 'ticket-category')
                $transactions->select('transactions.*')->leftJoin('tickets as ti', 'ti.id', '=', 'transactions.ticket_id')->orderBy('ti.category', $request->order);
            else if ($request->sort == 'user-name')
                $transactions->select('transactions.*')->leftJoin('users as us', 'us.id', '=', 'transactions.user_id')->orderBy('us.name', $request->order);
            else
                $transactions->orderBy($request->sort, $request->order);
        else
            $transactions->orderBy('updated_at', 'desc');

        return view('transaction.index', [
            'transactions' => $transactions->paginate($this->num)
        ]);
    }

    public function indexReports(Request $request)
    {
        if ($request->route()->getName() == 'report.dealers') {
            $view = 'transaction.index_dealer';
            $fields = ['created_at', 'user-name', 'ticket-event-name', 'ticket-category', 'price'];
        } else {
            $view = 'transaction.index_agent';
            $fields = ['created_at', 'user-name', 'ticket-event-name', 'ticket-category', 'price', 'user-percent'];
        }
        $this->validate($request, [
            'sort' => 'in:' . implode(',', $fields),
            'order' => 'required_with:sort|in:asc,desc'
        ]);

        $reports = Transaction::whereIn('transactions.user_id', Auth::user()->users()->pluck('id'));
        //$reports = Transaction::select('transactions.*')->leftJoin('tickets as ti', 'ti.id', '=', 'transactions.ticket_id')->whereIn('ti.user_id', Auth::user()->users()->pluck('id'));

        $search = [
            'fields' => [],
            'values' => []
        ];
        foreach ($fields as $field)
            if ($request->has($field)) {
                $search['fields'][] = str_replace('-', '.', $field);
                $search['values'][] = '*' . $request[$field] . '*';
            }

        if (!empty($search['fields']))
            $reports->search($search['values'], $search['fields']);

        if ($request->has('sort'))
            if ($request->sort == 'ticket-event-name')
                $reports->select('transactions.*')->leftJoin('tickets as ti', 'ti.id', '=', 'transactions.ticket_id')->leftJoin('events as ev', 'ev.id', '=', 'ti.event_id')->orderBy('ev.name', $request->order);
            else if ($request->sort == 'ticket-category')
                $reports->select('transactions.*')->leftJoin('tickets as ti', 'ti.id', '=', 'transactions.ticket_id')->orderBy('ti.category', $request->order);
            else if ($request->sort == 'user-name')
                $reports->select('transactions.*')->leftJoin('users as us', 'us.id', '=', 'transactions.user_id')->orderBy('us.name', $request->order);
            else if ($request->sort == 'user-percent')
                $reports->select('transactions.*')->leftJoin('users as us', 'us.id', '=', 'transactions.user_id')->orderBy('us.percent', $request->order);
            else
                $reports->orderBy($request->sort, $request->order);
        else
            $reports->orderBy('updated_at', 'desc');

        return view($view, [
            'reports' => $reports->paginate($this->num)
        ]);
    }
}
