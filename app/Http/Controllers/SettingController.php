<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\UserFile;
use App\Setting;
use Auth;

class SettingController extends Controller
{
    public function index()
    {
        $settings = array();
        $settings["percent"] = Setting::where('name', 'percent')->first()->value;
        $settings["days"] = Setting::where('name', 'days')->first()->value;
        $settings["balance"] = Auth::user()->balance;

        return view('settings', array('settings' => $settings));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'percent' => 'integer',
            'days' => 'integer'
        ]);

        $percent = Setting::where('name', 'percent')->first();
        $percent->value = $request->percent;
        $percent->save();

        $days = Setting::where('name', 'days')->first();
        $days->value = $request->days;
        $days->save();

        if ($request->hasFile('file')) {
            $file = $request->file;

            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            Storage::disk('public')->put($filename, File::get($file));

            $userFile = new UserFile;
            $userFile->original_filename = $file->getClientOriginalName();
            $userFile->filename = $filename;
            $userFile->mime_type = $file->getClientMimeType();
            $userFile->size = Storage::disk('public')->size($filename);

            $userFile->save();

            $agreementFile = Setting::where('name', 'user_agreement')->first();
            if ($agreementFile->value) {
                $oldFile = UserFile::find($agreementFile->value);
                File::delete(storage_path('app/public') . '/' . $oldFile->filename);
                $oldFile->delete();
            }
            $agreementFile->value = $userFile->id;

            $agreementFile->save();
        }

        return redirect()->back();
    }
}