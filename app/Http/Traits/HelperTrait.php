<?php

namespace App\Http\Traits;

trait HelperTrait
{
    public static function checkEntity($entity, $relations, $onlyCheck = false)
    {
        foreach ($relations as $relation)
            if ($entity[$relation]->count()) {
                if ($onlyCheck)
                    return false;
                else {
                    $errorText = 'Сущность не может быть удалена.';
                    return abort(422, $errorText, ['errorText' => $errorText]);
                }
            }

        return true;
    }
}