<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::pattern('id', '[0-9]+');
Route::pattern('item', '(new)|([0-9]+)');

Route::get('/', function () {
    if (Auth::check())
        return redirect('/personal');
    else
        return redirect('/login');
})->name('welcome');

Route::post('/forgot', [
    'uses' => 'Auth\PasswordController@postSignUp',
    'as' => 'forgot'
]);

Route::post('/signup', [
    'uses' => 'UserController@postSignUp',
    'as' => 'signup'
]);

Route::post('/signin', [
    'uses' => 'UserController@postSignIn',
    'as' => 'signin'
]);

Route::get('/logout', function () {
    Auth::logout();
    return redirect('login');
});

Route::get('/pagination', [
    'uses' => 'HomeController@setNumber'
]);

Route::get('/countries', 'CountryController@getCountries');

Route::get('/cities/{id}', 'CountryController@getCities');

Route::get('/images/{filename}', 'UserFileController@getFile');

Route::auth();

Route::get('/register', [
    'uses' => 'UserController@register',
    'as' => 'register'
]);

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function() {

    Route::get('/personal', 'UserController@indexPersonal');
    Route::post('/personal', 'UserController@storePersonal');

    Route::get('/objects', ['uses' => 'ObjectController@index', 'as' => 'objects']);
    Route::get('/object/{item}', 'ObjectController@view');

    Route::get('/events', ['uses' => 'EventController@index', 'as' => 'events']);
    Route::get('/event/{item}', 'EventController@view');
    Route::get('/event/tickets/{id}', 'EventController@tickets');

});

Route::group(['middleware' => 'role:customer|dealer|agent'], function() {

    Route::get('/tickets', ['uses' => 'TicketSeatController@index', 'as' => 'tickets']);
    Route::get('/ticket/{id}', 'TicketSeatController@view');
    Route::post('/tickets/preoccupy', ['uses' => 'TicketSeatController@preOccupy']);
    Route::post('/tickets/occupy', ['uses' => 'TicketSeatController@occupy']);

    Route::get('/occupied', ['uses' => 'TicketSeatController@index', 'as' => 'occupied']);

    Route::post('/ticket/free/{id}', 'TicketSeatController@free');

    //Route::get('/object/events/{id}', 'ObjectController@events');

});

Route::group(['middleware' => 'role:stickler'], function() {

    Route::get('/users', ['uses' => 'UserController@index', 'as' => 'users']);
    Route::get('/user/{item}', 'UserController@view');
    Route::post('/user/save', 'UserController@store');
    Route::get('/user/delete/{id}', 'UserController@delete');

    Route::get('/settings', 'SettingController@index');
    Route::post('/settings', 'SettingController@store');

    Route::get('/transactions', ['uses' => 'TransactionController@index', 'as' => 'transactions']);

    Route::get('/mailing', ['uses' => 'MailingController@index', 'as' => 'mailing']);
    Route::post('/mailing', 'MailingController@send');

});

Route::group(['middleware' => 'role:stickler|seller'], function() {

    Route::post('/object/save', 'ObjectController@store');
    Route::get('/object/delete/{id}', 'ObjectController@delete');

    Route::post('/event/save', 'EventController@store');
    Route::get('/event/delete/{id}', 'EventController@delete');

    Route::post('/halls', ['uses' => 'ObjectController@halls', 'as' => 'halls']);
    Route::get('/hall/{item}', 'HallController@view');
    Route::post('/hall/data', ['uses' => 'HallController@data']);
    Route::post('/hall', ['uses' => 'HallController@store']);

    Route::group(['prefix' => 'stickler'], function() {

        Route::get('/tickets', ['uses' => 'TicketController@index', 'as' => 'stickler.tickets']);
        Route::get('/ticket/{item}', 'TicketController@view');
        Route::post('/ticket/save', 'TicketController@store');
        Route::get('/ticket/delete/{id}', 'TicketController@delete');
        Route::post('/ticket/hall', 'TicketController@hall');

    });

});

Route::group(['middleware' => 'role:seller'], function() {

    Route::resource('dealers', 'DealerController', ['except' => [
        'create', 'edit', 'update', 'destroy'
    ]]);
    Route::get('/dealers/delete/{id}', 'DealerController@delete');

    Route::get('/report/dealers', ['uses' => 'TransactionController@indexReports', 'as' => 'report.dealers']);

});

Route::group(['middleware' => 'role:dealer'], function() {

    Route::resource('agents', 'AgentController', ['except' => [
        'create', 'edit', 'update', 'destroy'
    ]]);
    Route::get('/agents/delete/{id}', 'AgentController@delete');

    Route::get('/report/agents', ['uses' => 'TransactionController@indexReports', 'as' => 'report.agents']);

});

/*Route::group(['middleware' => 'role:dealer'], function() {

    Route::get('/agents', ['uses' => 'UserController@indexAgents', 'as' => 'agents']);
    Route::get('/agent/{item}', 'UserController@viewAgents');
    Route::post('/agent/save', 'UserController@storeAgents');
    Route::get('/agent/delete/{id}', 'UserController@deleteAgents');

});*/

