<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'name' => 'Российская Федерация'
        ]);

        DB::table('cities')->insert([
            'country_id' => 1,
            'name' => 'Москва'
        ]);
        DB::table('cities')->insert([
            'country_id' => 1,
            'name' => 'Санкт-Петербург'
        ]);

        DB::table('roles')->insert([
            'name' => 'customer',
            'display_name' => 'Покупатель',
            'description' => 'Покупатель',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'seller',
            'display_name' => 'Продавец',
            'description' => 'Продавец',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'dealer',
            'display_name' => 'Дилер',
            'description' => 'Дилер',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'agent',
            'display_name' => 'Агент',
            'description' => 'Агент',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'stickler',
            'display_name' => 'Стиклер',
            'description' => 'Стиклер',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'name' => 'customer',
            'email' => 'customer@customer.ru',
            'days' => 7,
            'percent' => 10,
            'balance' => 10,
            'agreement' => false,
            'status' => false,
            'country_id' => 1,
            'city_id' => 1,
            'dealer_id' => NULL,
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'name' => 'seller',
            'email' => 'seller@seller.ru',
            'days' => 7,
            'percent' => 10,
            'balance' => 20,
            'agreement' => false,
            'status' => false,
            'country_id' => 1,
            'city_id' => 1,
            'dealer_id' => NULL,
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'name' => 'dealer',
            'email' => 'dealer@dealer.ru',
            'days' => 7,
            'percent' => 10,
            'balance' => 40,
            'agreement' => false,
            'status' => true,
            'country_id' => 1,
            'city_id' => 1,
            'dealer_id' => NULL,
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'name' => 'agent',
            'email' => 'agent@agent.ru',
            'days' => 7,
            'percent' => 10,
            'balance' => 30,
            'agreement' => false,
            'status' => false,
            'country_id' => 1,
            'city_id' => 1,
            'dealer_id' => 3,
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'name' => 'stickler',
            'email' => 'admin@admin.ru',
            'days' => 7,
            'percent' => 10,
            'balance' => 50,
            'agreement' => false,
            'status' => false,
            'country_id' => 1,
            'city_id' => 1,
            'dealer_id' => NULL,
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '1'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '2',
            'role_id' => '2'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '3',
            'role_id' => '3'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '4',
            'role_id' => '4'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '5',
            'role_id' => '5'
        ]);

        DB::table('objects')->insert([
            'name' => 'Большой театр',
            'country_id' => 1,
            'city_id' => 1,
            'address' => 'ул. Театральная, 1',
            'description' => 'Большой театр',
            'user_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('objects')->insert([
            'name' => 'Александринский театр',
            'country_id' => 1,
            'city_id' => 2,
            'address' => 'пл. Островского, 6',
            'description' => 'Александринский театр',
            'user_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('events')->insert([
            'name' => 'Спектакль 1',
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'object_id' => 1,
            'user_id' => 5,
            'description' => 'Спектакль 1 в Москве',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('events')->insert([
            'name' => 'Спектакль 2',
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'object_id' => 2,
            'user_id' => 5,
            'description' => 'Спектакль 1 в Санкт-Петербурге',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('settings')->insert([
            'name' => 'percent',
            'value' => '10',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'name' => 'user_agreement',
            'value' => '0',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'name' => 'days',
            'value' => '7',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}