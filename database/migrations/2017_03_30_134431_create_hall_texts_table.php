<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHallTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hall_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hall_id')->unsigned();
            $table->decimal('scale_x');
            $table->decimal('scale_y');
            $table->decimal('left', 8, 4);
            $table->decimal('top', 8, 4);
            $table->string('body');
            $table->timestamps();

            $table->foreign('hall_id')->references('id')->on('halls')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hall_texts');
    }
}
