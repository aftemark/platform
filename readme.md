[Дизайн приложения](fish.jpg?raw=true)

Пользователи
Стиклер admin@admin.ru 123456
Дилер dealer@dealer.ru 123456
Агент agent@agent.ru 123456
Покупатель customer@customer.ru 123456
Продавец seller@seller.ru 123456

#PAGES
/login - Авторизация
/register - Регистрация

/personal - Страница личной инфорации

#Стиклер
/stickler - Список пользователей

/stickler/objects - Список объектов
+ создание изменение удаление

/stickler/events - Список мероприятий
+ создание изменение удаление

/stickler/settings - Настройки

#Дилер
/dealer/agents - Список агентов
+ создание изменение удаление

#Агент
/agent/objects - Список объектов
+ просмотр мероприятий

/agent/events - Список мероприятий

#Покупатель
/customer/objects - Список объектов
+ просмотр мероприятий

/customer/events - Список мероприятий

#Продавец
/seller/objects - Список объектов
+ создание изменение удаление

/seller/events - Список мероприятий
+ создание изменение удаление