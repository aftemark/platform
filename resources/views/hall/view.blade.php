@extends('layouts.app')

@section('title')Зал @endsection

@section('content')
    <div class="container">
        <form class="row" id="hallInfo">
            <div class="col-md-8">
                <input type="text" class="form-control" name="name" placeholder="Название зала" value="{{ $hall->name or '' }}"{{ $allowSave ? ' required' : ' disabled' }}>
            </div>
            @if($allowSave)
            <button class="btn btn-success col-md-2">Сохранить</button>
            @endif
            @if(Request::has('object'))
            <input type="hidden" name="object" value="{{ Request::get('object') }}">
            @endif
            @if($hall->id)
            <input type="hidden" name="id" value="{{ $hall->id }}">
            @endif
            {{ csrf_field() }}
            <a href="{{ url('/' . Auth::user()->main_role->name . '/object/' . ($hall->id ? $hall->object_id : Request::get('object'))) }}" class="btn btn-danger col-md-2">Отменить</a>
        </form>
        <hr>
        <div class="row">
            <div class="col-md-9">
                <div style="width:804px;margin: 0 auto;border: 1px solid #aaa;" id="canvas-wrapper">
                    <canvas id="canvas" width="800" height="690"></canvas>
                </div>
            </div>
            <div class="col-md-3">
                @if($allowSave)
                <div class="form-group">
                    <button class="btn btn-success" id="addSeats" data-toggle="modal" data-target="#placesModal">Добавить места</button>
                </div>
                <div class="form-group hidden" id="seatInfo">
                    <label>Номер места</label>
                    <input type="number" class="form-control" name="num" value="1">
                    <label>Ряд места</label>
                    <input type="number" class="form-control" name="row" value="1">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" id="addText" onclick="addText()">Добавить текст</button>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" id="deleteSeat" onclick="removeObjects()">Удалить</button>
                </div>
                @endif
            </div>
        </div>
    </div>

    @if($allowSave)
    <div class="modal fade" id="placesModal" tabindex="-1" role="dialog" aria-labelledby="placesModalLabel">
        <div class="modal-dialog" role="document">
            <form class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="placesModalLabel">Добавление мест</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Количество мест</label>
                        <br>
                        <label>Количество рядов</label>
                        <input type="number" class="form-control" name="rows" value="1">
                        <label>Количество мест в ряду</label>
                        <input type="number" class="form-control" name="seats" value="1">
                    </div>
                    <div class="form-group">
                        <label>Начинать нумерацию рядов с</label>
                        <input type="number" class="form-control" name="rows_start" placeholder="Начинать отсчет с" value="1">
                    </div>
                    <div class="form-group">
                        <label>Начинать нумерацию мест с</label>
                        <input type="number" class="form-control" name="seats_start" placeholder="Начинать отсчет с" value="1">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Добавить</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script src="{{ url('src/js/fabric/dist/fabric.min.js') }}"></script>
    <script src="{{ url('src/js/scheme.js') }}"></script>
    <script>
        var canvas = new fabric.Canvas('canvas');
        const displayOnly = {{ $allowSave ? 'false' : 'true' }};
        schemeInit();
        /*canvas.observe('object:moving', function (e) {
         var obj = e.target;
         if (obj.getHeight() > obj.canvas.height || obj.getWidth() > obj.canvas.width){
         obj.setScaleY(obj.originalState.scaleY);
         obj.setScaleX(obj.originalState.scaleX);
         }
         obj.setCoords();
         if (obj.getBoundingRect().top - (obj.cornerSize / 2) < 0 || obj.getBoundingRect().left -  (obj.cornerSize / 2) < 0) {
         obj.top = Math.max(obj.top, obj.top-obj.getBoundingRect().top + (obj.cornerSize / 2));
         obj.left = Math.max(obj.left, obj.left-obj.getBoundingRect().left + (obj.cornerSize / 2));
         }
         if (obj.getBoundingRect().top+obj.getBoundingRect().height + obj.cornerSize  > obj.canvas.height || obj.getBoundingRect().left + obj.getBoundingRect().width + obj.cornerSize  > obj.canvas.width) {
         obj.top = Math.min(obj.top, obj.canvas.height-obj.getBoundingRect().height+obj.top-obj.getBoundingRect().top - obj.cornerSize / 2);
         obj.left = Math.min(obj.left, obj.canvas.width-obj.getBoundingRect().width+obj.left-obj.getBoundingRect().left - obj.cornerSize /2);
         }
         });*/
        (function () {
            $('#placesModal form').submit(function (e) {
                var params = {},
                        objects = [],
                        xPos = rectSize,
                        yPos = rectSize;

                $.each($(this).serializeArray(), function (_, obj) {
                    params[obj.name] = obj.value ? parseInt(obj.value) : 1;
                });

                for (let row = params.rows_start; row < params.rows_start + params.rows; row++) {
                    for (let num = params.seats_start; num < params.seats_start + params.seats; num++) {
                        let group = addRect(yPos, xPos, row, num);

                        objects.push(group);
                        group.set('active', true);
                        yPos += rectSize + rectSize / 6;
                    }
                    yPos = rectSize;
                    xPos += rectSize + rectSize / 6;
                }

                let selectGroup = new fabric.Group(objects, {
                    originX: 'center',
                    originY: 'center',
                    hasControls: false
                });

                canvas._activeObject = null;
                canvas.setActiveGroup(selectGroup.setCoords()).renderAll();

                $(this).closest('.modal').modal('hide');
                e.preventDefault();
            });

            $('#hallInfo').submit(function (e) {
                let canvasData = canvas.toJSON(),
                        formData = $(this).serializeArray(),
                        validate = [],
                        fails = [],
                        count = 0;

                if (canvasData.objects) {
                    canvasData.objects.forEach(function (object) {
                        let seat = object.row + '-' + object.num;
                        if (!object.num)
                            return true;
                        if ($.inArray(seat, validate) == -1)
                            validate.push(seat);
                        else
                            fails.push(seat);
                    });
                }

                if (fails.length > 0)
                    alert('Удалите повторяющиеся места - ' + fails.join(', ') + '.');
                else {
                    if (canvasData.objects) {
                        canvasData.objects.forEach(function (object) {
                            if (object.left >= 0 && object.left <= 700 && object.top >= 0 && object.top <= 810) {
                                if (object.type == 'group') {
                                    formData.push({name: "seat[left][" + count + "]", value: object.left});
                                    formData.push({name: "seat[top][" + count + "]", value: object.top});
                                    formData.push({name: "seat[number][" + count + "]", value: object.num});
                                    formData.push({name: "seat[row][" + count + "]", value: object.row});
                                    count++;
                                } else if (object.type == 'i-text') {
                                    formData.push({name: "text[left][" + count + "]", value: object.left});
                                    formData.push({name: "text[top][" + count + "]", value: object.top});
                                    formData.push({name: "text[scaleX][" + count + "]", value: object.scaleX});
                                    formData.push({name: "text[scaleY][" + count + "]", value: object.scaleY});
                                    formData.push({name: "text[text][" + count + "]", value: object.text});
                                    count++;
                                }
                            }
                        });
                    }
                }

                $.post("/hall", formData).done(function(data) {
                    window.location.href = data.url;
                });

                e.preventDefault();
            });
        })();
        @if ($hall->id)
        $.post("{{ url('/hall/data') }}", $('#hallInfo').serialize()).success(function (data) {
            $.each(data.seats, function (_, seat) {
                addRect(seat.left, seat.top, seat.row, seat.seat);
            });
            $.each(data.texts, function (_, text) {
                addText(text.body, text.left, text.top, text.scale_x, text.scale_y);
            });
        });
        @endif
    </script>
@endsection