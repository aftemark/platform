@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h3>Регистрация</h3>
                <ul class="nav nav-tabs">
                    <li id="customerSelect"{{ Request::old('type') == 'agent' ? '' : ' class=active' }}><a data-toggle="tab" href="#customer">Покупатель</a></li>
                    <li id="agentSelect"{{ Request::old('type') == 'agent' ? ' class=active' : '' }}><a data-toggle="tab" href="#agent">Агент</a></li>
                </ul>
                <div class="tab-content">
                    <div id="customer" class="tab-pane fade{{ Request::old('type') == 'agent' ? '' : ' in active' }}">
                        <form class="form-horizontal" action="{{ url('/signup') }}" method="POST">
                            <div class="form-group{{ $errors->has('name') && Request::old('type') == 'customer' ? ' has-error' : '' }}">
                                <input class="form-control" type="text" name="name" placeholder="Имя" value="{{ Request::old('name') }}">
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group{{ $errors->has('phone') && Request::old('type') == 'customer' ? ' has-error' : '' }}">
                                    <input class="form-control" type="text" name="phone" placeholder="Телефон" value="{{ Request::old('phone') }}">
                                </div>
                                <div class="col-md-6 form-group{{ $errors->has('email') && Request::old('type') == 'customer' ? ' has-error' : '' }}">
                                    <input class="form-control" type="text" name="email" placeholder="E-mail" value="{{ Request::old('email') }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group{{ $errors->has('country') && Request::old('type') == 'customer' ? ' has-error' : '' }}">
                                    <label for="customer_phone">Страна</label>
                                    <select class="form-control" id="customer_country" data-city="#customer_city" name="country"></select>
                                </div>
                                <div class="col-md-6 form-group {{ $errors->has('city') && Request::old('type') == 'customer' ? ' has-error' : '' }}">
                                    <label for="customer_city">Город</label>
                                    <select class="form-control" id="customer_city" name="city">
                                        <option value="">Выберите город</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group{{ $errors->has('password') && Request::old('type') == 'customer' ? ' has-error' : '' }}">
                                    <input class="form-control" type="password" name="password" placeholder="Пароль">
                                </div>
                                <div class="col-md-6 form-group{{ ($errors->has('password') || $errors->has('password_confirmation')) && Request::old('type') == 'customer' ? ' has-error' : '' }}">
                                    <input class="form-control" type="password" name="password_confirmation" placeholder="Пароль еще раз">
                                </div>
                            </div>
                            <input type="hidden" name="type" value="customer">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <a class="btn btn-primary agreementSubmit">Зарегистрироваться</a>
                        </form>
                    </div>
                    <div id="agent" class="tab-pane fade{{ Request::old('type') == 'agent' ? ' in active' : '' }}">
                        <form class="form-horizontal" action="{{ url('/signup') }}" method="post">
                            <div class="form-group{{ $errors->has('name') && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                <input class="form-control" type="text" name="name" placeholder="Имя" value="{{ Request::old('name') }}">
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group{{ $errors->has('phone') && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                    <input class="form-control" type="text" name="phone" placeholder="Телефон" value="{{ Request::old('phone') }}">
                                </div>
                                <div class="col-md-6 form-group{{ $errors->has('email') && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                    <input class="form-control" type="text" name="email" placeholder="E-mail" value="{{ Request::old('email') }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group{{ $errors->has('country') && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                    <label for="agent_country">Страна</label>
                                    <select class="form-control" id="agent_country" data-city="#agent_city" name="country"></select>
                                </div>
                                <div class="col-md-6 form-group{{ $errors->has('city') && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                    <label for="agent_city">Город</label>
                                    <select class="form-control" id="agent_city" name="city">
                                        <option value="">Выберите город</option>
                                    </select>
                                </div>
                            </div>
                            {{--<div class="form-group{{ $errors->has('dealer') && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                <label for="dealer">Дилер</label>
                                <select class="form-control" id="dealer" name="dealer">
                                    <option value="false">Нет</option>
                                    @foreach($dealers as $dealer)
                                        <option value="{{ $dealer->id }}">{{ $dealer->name }}</option>
                                    @endforeach
                                </select>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-6 form-group{{ $errors->has('password') && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                    <input class="form-control" type="password" name="password" placeholder="Пароль">
                                </div>
                                <div class="col-md-6 form-group{{ ($errors->has('password') || $errors->has('password_confirmation')) && Request::old('type') == 'agent' ? ' has-error' : '' }}">
                                    <input class="form-control" type="password" name="password_confirmation" placeholder="Пароль еще раз">
                                </div>
                            </div>
                            <input type="hidden" name="type" value="agent">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <a class="btn btn-primary agreementSubmit">Зарегистрироваться</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('includes.message-block')
    </div>
    <div class="modal fade" id="agreement" tabindex="-1" role="dialog" aria-labelledby="agreementLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="agreementLabel">Пользовательское соглашение</h4>
                </div>
                <div class="modal-body">
                    <p>Пользовательское соглашение</p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input id="agreementCheckbox" type="checkbox">
                                    Согласен с условиями
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pull-right">
                            <button id="submitForm" disabled class="btn btn-success">Ок</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('includes.city_country_script')
    <script>
    $( document ).ready(function() {
        @if(Session::has('type'))
        $("#{{Session::get('type')}}Select").eq(0).trigger( "click" );
        @endif
        $(".agreementSubmit").on('click', function () {
            $('#agreement').modal('show');
        });

        $('#agreementCheckbox').click(function() {
            if ($(this).is(':checked')) {
                $('#submitForm').removeAttr('disabled');
            } else {
                $('#submitForm').attr('disabled', 'disabled');
            }
        });

        $('#submitForm').click(function() {
            if ($("#customerSelect").hasClass('active'))
                $('#customer form').submit();
            else if ($("#agentSelect").hasClass('active'))
                $('#agent form').submit();
        });
    });
    </script>
@endsection
