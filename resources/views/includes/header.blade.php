<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/login') }}">Платформа</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @role('stickler')
                <li><a href="{{ url('/stickler/tickets') }}">Билеты</a></li>
                <li><a href="{{ url('/users') }}">Пользователи</a></li>
                <li><a href="{{ url('/transactions') }}">Транзакции</a></li>
                <li><a href="{{ url('/objects') }}">Объекты</a></li>
                <li><a href="{{ url('/events') }}">Мероприятия</a></li>
                <li><a href="{{ url('/mailing') }}">Рассылки</a></li>
                <li><a href="{{ url('/settings') }}">Настройки</a></li>
                @endrole
                @role('customer')
                <li><a href="{{ url('/tickets') }}">Билеты</a></li>
                <li><a href="{{ url('/objects') }}">Объекты</a></li>
                <li><a href="{{ url('/events') }}">Мероприятия</a></li>
                <li><a href="{{ url('/occupied') }}">Список купленных/забронированных билетов</a></li>
                @endrole
                @role('seller')
                <li><a href="{{ url('/stickler/tickets') }}">Билеты</a></li>
                <li><a href="{{ url('/events') }}">Мероприятия</a></li>
                <li><a href="{{ url('/objects') }}">Объекты</a></li>
                <li><a href="{{ url('/dealers') }}">Дилеры</a></li>
                <li><a href="{{ url('/report/dealers') }}">Отчет по работе дилеров</a></li>
                @endrole
                @role('dealer')
                <li><a href="{{ url('/tickets') }}">Билеты</a></li>
                <li><a href="{{ url('/agents') }}">Агенты</a></li>
                <li><a href="{{ url('/objects') }}">Объекты</a></li>
                <li><a href="{{ url('/events') }}">Мероприятия</a></li>
                <li><a href="{{ url('/occupied') }}">Список купленных/забронированных билетов</a></li>
                <li><a href="{{ url('/report/agents') }}">Отчет по работе агентов</a></li>
                @endrole
                @role('agent')
                <li><a href="{{ url('/tickets') }}">Билеты</a></li>
                <li><a href="{{ url('/objects') }}">Объекты</a></li>
                <li><a href="{{ url('/events') }}">Мероприятия</a></li>
                <li><a href="{{ url('/occupied') }}">Список купленных/забронированных билетов</a></li>
                @endrole
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/personal') }}">{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}</a></li>
                <li><a href="{{ url('/logout') }}">Выйти</a></li>
            </ul>
        </div>
    </div>
</nav>