<script>
    $(document).ready(function() {
        var formCountryID = '@if(isset($object["country_id"])){{ $object["country_id"] }}@endif';
        var formCityID = '@if(isset($object["city_id"])){{ $object["city_id"] }}@endif';

        $.get("/countries", function(data) {
            var countrySelect = $("[name=country]");
            countrySelect.empty();
            countrySelect.append('<option value="">Выберите страну</option>');
            $.each(data, function() {
                $("[name=country]").append('<option value="' + this.id + '">' + this.name + '</option>');
            });
            if (formCountryID != '') {
                countrySelect.val(formCountryID);
                countrySelect.trigger("change");
            }
        });

        $("[name=country]").on("change", function() {
            var countryId = $("option:selected", this).val(),
                dataCity = $(this).attr('data-city'),
                citySelect = dataCity ? $(dataCity) : $("[name=city]");
            if (countryId) {
                $.get("/cities/" + countryId, function (data) {
                    citySelect.empty();
                    $.each(data, function () {
                        citySelect.append('<option value="' + this.id + '">' + this.name + '</option>');
                    });
                    if (formCityID != '') {
                        citySelect.val(formCityID);
                        citySelect.trigger("change");
                    }
                });
            } else
                citySelect.empty().append('<option value="">Выберите город</option>');
        });
    });
</script>