@extends('layouts.app')

@section('title')
    Список мероприятий
@endsection

@section('content')
    <div class="container">
        <form class="row">
            <div class="row">
                <div class="col-md-6"><h3>Список мероприятий</h3></div>
                @if(Auth::user()->hasRole(['stickler', 'seller']))
                <div class="col-md-6 pull-right"><a href="{{ url('/event/new') }}" class="btn btn-primary">Добавить мероприятие</a></div>
                @endif
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th data-name="name">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Название
                    </th>
                    <th data-name="date">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'date' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'date' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Время
                    </th>
                    <th data-name="object_name">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'object_name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'object_name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Объект
                    </th>
                    <th data-name="description">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'description' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'description' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Описание
                    </th>
                    <th>
                        Фото
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="events">
                    <tr>
                        <td>
                            <input type="text" class="form-control" name="name" value="{{ Request::get('name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="date" value="{{ Request::get('date') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="object_name" value="{{ Request::get('object_name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="description" value="{{ Request::get('description') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach ($events as $event)
                    <tr>
                        @if(!empty($event->name))
                            <td>{{ $event->name }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(!empty($event->date))
                            <td>{{ $event->date }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(!empty($event->object))
                            <td>{{ $event->object->name }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(!empty($event->description))
                            <td>{{ $event->description }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(count($event->photos) > 0)
                            <td>
                                @foreach ($event->photos as $photo)
                                    <a href="{{ $photo }}">{{ $photo }}</a>
                                @endforeach
                            </td>
                        @else
                            <td>-</td>
                        @endif
                        <td>
                            <a href="{{ url('/event/' . $event->id ) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            @if(Auth::user()->hasRole(['stickler', 'seller']))
                            <a class="delete-entity" href="{{ url('/event/delete/' . $event->id ) }}"><span class="glyphicon glyphicon-trash"></span></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @if(count($events) < 1)
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $events->links() }}</div>
                @if(Request::has('page'))
                <input type="hidden" name="page" value="{{ Request::get('page') }}">
                @endif
                @if(Request::has('object_id'))
                <input type="hidden" name="object_id" value="{{ Request::get('object_id') }}">
                @endif
                <div class="col-md-6">
                    <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
        </form>
    </div>
    @if(Auth::user()->hasRole(['stickler', 'seller']))
    @include('includes.delete-entity')
    @endif
@endsection