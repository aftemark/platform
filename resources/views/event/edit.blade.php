@extends('layouts.app')

@section('title')
	Мероприятие
@endsection

@section('content')
<section>
	<div class="container">
		@include('includes.message-block')
		<form action="{{ url('/event/save') }}" enctype="multipart/form-data" method="POST">
			<div class="row">
				<div class="col-md-6">
					<h3>Мероприятие</h3>
				</div>
				<div class="col-md-6 pull-right">
					@if($allowSave)
					<button type="submit" class="btn btn-success">Сохранить</button>
					@endif
					<a href="{{ url('/events') }}" class="btn btn-danger">Отменить</a>
					<input type="hidden" name="_token" value="{{ Session::token() }}">
					@if(isset($event["id"]))
						<input type="hidden" name="id" value="{{ $event["id"] }}">
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<input required name="name" type="text" class="form-control" placeholder="Название" value="{{ $event["name"] }}">
					</div>
					<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
						<input required name="date" type="text" class="form-control" placeholder="Время" value="{{ $event["date"] }}">
						<p>формат: 2016-06-17 14:52:05</p>
					</div>
					<div class="form-group{{ $errors->has('object') ? ' has-error' : '' }}">
						<label for="object">Объект</label>
						<select class="form-control" id="object" name="object" data-url="{{ url('/halls') }}" data-id="#hall">
							<option value="false">Выберите объект</option>
							@foreach ($objects as $objectId => $objectName)
								<option {{ $objectId == $event->object_id ? 'selected ' : '' }}value="{{ $objectId }}">{{ $objectName }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group{{ $errors->has('hall') ? ' has-error' : '' }}">
						<label for="hall">Зал</label>
						<select class="form-control" id="hall" name="hall">
							<option value="false">Выберите зал</option>
							@if($event->object)
							@foreach ($event->object->halls as $hall)
							<option {{ $hall->id == $event->hall_id ? 'selected ' : '' }}value="{{ $hall->id }}">{{ $hall->name }}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
						<textarea rows="7" name="description" class="form-control">{{ $event["description"] }}</textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
					<label for="fileupload">Добавить фотографии</label>
					<input multiple class="form-control" id="fileupload" type="file" name="files[]">
				</div>
			</div>
			@if(count($event->links) > 0)
				<div class="row">
					<div class="col-md-12 checkbox">
						<label>
							<input name="remove" type="checkbox" value="remove">
							Удалить изображения
						</label>
					</div>
				</div>
				<div class="row">
					@foreach ($event->links as $link)
						<div class="col-md-3">
							<img src="{{ $link }}">
						</div>
					@endforeach
				</div>
			@endif
		</form>
	</div>
</section>
@endsection

@section('script')
	<script>
		(function () {
			loadElements('#object');
			@if(!$allowSave)
			$('form').find('input, select, textarea').attr('disabled', true);
			@endif
		})();
	</script>
@endsection