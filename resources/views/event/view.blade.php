@extends('layouts.app')

@section('title')Мероприятие @endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Мероприятие</h3>
            </div>
            <div class="col-md-6 pull-right">
                <a class="btn btn-success" href="{{ route('tickets', ['event_id' => $event->id]) }}">Билеты</a>
                <a href="{{ url('/events') }}" class="btn btn-danger">Назад</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Название</label>
                    <input disabled type="text" class="form-control" value="{{ $event->name }}">
                </div>
                <div class="form-group">
                    <label>Дата</label>
                    <input disabled type="text" class="form-control" value="{{ $event->date }}">
                </div>
                <div class="form-group">
                    <label>Объект</label>
                    <input disabled type="text" class="form-control" value="{{ $event->object->name }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <textarea rows="7" class="form-control">{{ $event->description }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($event->links as $link)
            <div class="col-md-3">
                <img src="{{ $link }}">
            </div>
            @endforeach
        </div>
    </div>
    <div class="modal fade" id="tickets" tabindex="-1" role="dialog" aria-labelledby="ticketsLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ticketsLabel">Билеты на мероприятие</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ок</button>
                </div>
            </div>
        </div>
    </div>
@endsection