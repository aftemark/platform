@php $summary = 0; @endphp
@foreach($tickets as $ticket)
<div class="row" data-cost="{{ $ticket->ticket->sell_price }}">
    <div class="col-md-6">
        <p>{{ $ticket->ticket->event->name }} - {{ $ticket->ticket->category }} - {{ $ticket->seat->row }} - {{ $ticket->seat->seat }}</p>
    </div>
    <div class="col-md-4">
        <p>{{ $ticket->ticket->sell_price }} р.</p>
    </div>
    <div class="col-md-2">
        <a href="#" class="remove-ticket">x</a>
    </div>
    <input type="hidden" name="tickets[]" value="{{ $ticket->id }}">
    @php $summary+= $ticket->ticket->sell_price; @endphp
</div>
@endforeach
{{ csrf_field() }}
<input type="hidden" name="occupy_type" value="purchased">