@extends('layouts.app')

@section('title')Список билетов @endsection

@php $routeName = Request::route()->getName(); @endphp

@section('content')
    <div class="container">
        <form class="row">
            <div class="row">
                <div class="col-md-6">
                    @if($routeName == 'tickets')
                    <h3>Список билетов</h3>
                    @else
                    <h3>Список купленных/забронированных билетов</h3>
                    @endif
                </div>
                <div class="col-md-6 pull-right">
                    @if($routeName == 'tickets')
                    <label class="radio-inline"><input type="radio" name="occupy_type" value="purchased">Купить</label>
                    @if(Auth::user()->hasRole(['agent', 'dealer']))
                    <label class="radio-inline"><input type="radio" name="occupy_type" value="booked">Бронировать</label>
                    @endif
                    <button disabled id="occupy" class="btn btn-success">Применить</button>
                    @endif
                </div>
            </div>
            <table id="tickets" class="table table-striped">
                <thead>
                    <tr>
                        @if($routeName == 'tickets')
                        <th>Выбрать</th>
                        @endif
                        <th data-name="ticket-event-name">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'ticket-event-name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'ticket-event-name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Мероприятие
                        </th>
                        <th data-name="ticket-category">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'ticket-category' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'ticket-category' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Категория билетов
                        </th>
                        <th data-name="ticket-sell_price">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'ticket-sell_price' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'ticket-sell_price' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Цена билетов
                        </th>
                        <th data-name="seat-row">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'seat-row' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'seat-row' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Ряд
                        </th>
                        <th data-name="seat-seat">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'seat-seat' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'seat-seat' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Место
                        </th>
                        <th data-name="status">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'status' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'status' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Статус
                        </th>
                        @if($routeName == 'occupied')
                        <th></th>
                        @endif
                    </tr>
                </thead>
                <tbody id="objects">
                    <tr>
                        @if($routeName == 'tickets')
                        <td></td>
                        @endif
                        <td>
                            <input type="text" class="form-control" name="ticket-event-name" value="{{ Request::get('ticket-event-name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="ticket-category" value="{{ Request::get('ticket-category') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="ticket-sell_price" value="{{ Request::get('ticket-sell_price') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="seat-row" value="{{ Request::get('seat-row') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="seat-seat" value="{{ Request::get('seat-seat') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach ($tickets as $ticket)
                    <tr{{ $ticket->allow_booking ? '' : ' disable-booking=true' }}>
                        @if($routeName == 'tickets')
                        <td>
                            <input type="checkbox" name="tickets[]" value="{{ $ticket->id }}">
                        </td>
                        @endif
                        <td>{{ $ticket->ticket->event->name }}</td>
                        <td>{{ $ticket->ticket->category}}</td>
                        <td>{{ $ticket->ticket->sell_price }}</td>
                        <td>{{ $ticket->seat->row }}</td>
                        <td>{{ $ticket->seat->seat }}</td>
                        @if(!$ticket->transaction_id)
                        <td>Свободен</td>
                        @elseif(is_null($ticket->transaction->price))
                        @if(Carbon\Carbon::parse($ticket->transaction->booked_till)->isFuture())
                        <td>Бронь</td>
                        @else
                        <td>Свободен</td>
                        @endif
                        @else
                        <td>Куплен</td>
                        @endif
                        <td>
                        @if($routeName == 'occupied')
                            <a class="share-ticket" href="{{ url('/ticket/test/' . $ticket->id) }}"><span class="glyphicon glyphicon-share"></span></a>
                            <a class="free-ticket" href="{{ url('/ticket/free/' . $ticket->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                        @else
                            <a href="{{ url('/ticket/' . $ticket->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                    @if(count($tickets) < 1)
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $tickets->links() }}</div>
                @if(Request::has('page'))
                <input type="hidden" name="page" value="{{ Request::get('page') }}">
                @endif
                @if(Request::has('event_id'))
                <input type="hidden" name="event_id" value="{{ Request::get('event_id') }}">
                @endif
                <div class="col-md-6">
                    <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
        </form>
    </div>
    @if($routeName == 'occupied')
    <div class="modal fade" id="ticketModal" role="dialog">
        <div class="modal-dialog">
            <form class="modal-content" method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Удаление билета</h4>
                </div>
                <div class="modal-body">
                    Вы действительно хотите удалить билет?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Да</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </form>
        </div>
    </div>
    @else
    <div class="modal fade" id="ticketModal" role="dialog">
        <div class="modal-dialog">
            <form action="{{ url('/tickets/occupy') }}" class="modal-body" method="POST"></form>
        </div>
    </div>
    @endif
@endsection

@section('script')
<script>
    (function () {
        @if($routeName == 'tickets')
        $('input[name=occupy_type]').on('change', function() {
            let occupyType = $('input[name=occupy_type]:checked').val();
            $('#occupy').attr('disabled', false);
            $('#tickets tbody tr').each(function () {
                if (occupyType == 'booked') {
                    if ($(this).attr('disable-booking') == 'true')
                        $(this).find('input').attr('disabled', true);
                } else
                    $(this).find('input').attr('disabled', false);
            });
        });

        $('#occupy').on('click', function(e) {
            let type = $('input[name=occupy_type]:checked').val();
            $.post('{{ url('/tickets/preoccupy') }}', $(this).closest('form').append('<input type="hidden" name="_token" value="{{ Session::token() }}"/>').serialize(), function (data) {
                let $modal = $('#ticketModal');
                $modal.find('.modal-body').empty().append(data);
                ticketSummary();
                $modal.modal('show');
            });
            e.preventDefault();
        });

        $('body').on('click', '.remove-ticket', function () {
            $(this).closest('.row').remove();
            ticketSummary();
        });

        function ticketSummary() {
            let summary = 0,
            $modal = $('#ticketModal');
            $('#ticketSummary').remove();
            $modal.find('.occupied-ticket').each(function() {
                summary += parseFloat($(this).attr('data-cost'));
            });
            $modal.find('.modal-content').append('<p id="ticketSummary">Итого: ' + summary + ' р.</p>');
            if (!summary)
                $modal.modal('hide');
        }
        @else
        $('.share-ticket').on('click', function(e) {
            let $temp = $('<input class="hidden">');
            $("body").append($temp);
            $temp.val($(this).attr('href')).select();
            document.execCommand("copy");
            $temp.remove();
            e.preventDefault();
        });

        $('.free-ticket').on('click', function(e) {
            var $modal = $("#ticketModal");
            $modal.find('form').attr('action', $(this).attr('href'));
            $modal.modal();
            e.preventDefault();
        });
        @endif
    })();
</script>
@endsection