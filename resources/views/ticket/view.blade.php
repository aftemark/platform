@extends('layouts.app')

@section('title')Билеты @endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
        @include('includes.message-block')
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h3>Билет</h3>
                </div>
                <form action="{{ url('/tickets/occupy') }}" class="col-md-6 pull-right" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" id="purchased" class="btn btn-success">Купить</button>
                    <button type="submit" id="booked" class="btn btn-primary">Бронировать</button>
                    <a href="{{ url('/tickets') }}" class="btn btn-danger">Отменить</a>
                    <input type="hidden" name="tickets[]" value="{{ $ticket->id }}">
                </form>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Мероприятие</label>
                        <input disabled type="text" class="form-control" value="{{ $ticket->ticket->event->name }}">
                    </div>
                    <div class="form-group">
                        <label>Место</label>
                        <input disabled type="text" class="form-control" value="{{ $ticket->ticket->event->object->name }}">
                    </div>
                    <div class="form-group">
                        <label>Адрес</label>
                        <input disabled type="text" class="form-control" value="{{ $ticket->ticket->event->object->address }}">
                    </div>
                    <div class="form-group">
                        <label>Дата и время</label>
                        <input disabled type="text" class="form-control" value="{{ $ticket->ticket->event->date }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <p>
                        @if($ticket->seat_id)
                            Ряд: {{ $ticket->seat->row }}, Место: {{ $ticket->seat->seat }}
                        @else
                            -
                        @endif
                        </p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 form-group">
                            <label>Категория билета</label>
                            <input disabled type="text" class="form-control" value="{{ $ticket->ticket->category }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Стоимость</label>
                            <input disabled type="text" class="form-control" value="{{ $ticket->ticket->sell_price }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea disabled rows="7" id="description" class="form-control">{{ $ticket->ticket->description }}</textarea>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')
    <script>
        (function () {
            $('#purchased, #booked').on('click', function() {
                $(this).closest('form').append('<input type="hidden" name="occupy_type" value="' + this.id + '"/>');
            });
        })();
    </script>
@endsection