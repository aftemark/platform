<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">{{ $occupy_type == 'purchased' ? 'Покупка' : 'Бронирование' }}</h4>
</div>
<div class="modal-content">
    @php $summary = 0; @endphp
    @foreach($ticketTemplates as $ticketTemplate)
    <div class="row">
        <p>{{ $ticketTemplate['name'] }}</p>
        @foreach($ticketTemplate['tickets'] as $ticket)
        <div class="row occupied-ticket" data-cost="{{ $ticket->ticket->sell_price }}">
            <div class="col-md-6">
                <p>{{ $ticket->seat->row }} - {{ $ticket->seat->seat }}</p>
            </div>
            <div class="col-md-4">
                <p>{{ $ticket->ticket->sell_price }} р.</p>
            </div>
            <div class="col-md-2">
                <a href="#" class="remove-ticket">x</a>
            </div>
            <input type="hidden" name="tickets[]" value="{{ $ticket->id }}">
            @php $summary += $ticket->ticket->sell_price; @endphp
        </div>
        @endforeach
    </div>
    <hr>
    @endforeach
    {{ csrf_field() }}
    <input type="hidden" name="occupy_type" value="{{ $occupy_type }}">
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-success">{{ $occupy_type == 'purchased' ? 'Перейти к оплате' : 'Бронировать' }}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
</div>