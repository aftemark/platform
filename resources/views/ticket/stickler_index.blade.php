@extends('layouts.app')

@section('title')Список билетов @endsection

@section('content')
    <div class="container">
        <form class="row">
            <div class="row">
                <div class="col-md-6"><h3>Список билетов</h3></div>
                <div class="col-md-6 pull-right">
                    <a href="{{ url('/stickler/ticket/new') }}" class="btn btn-primary">Добавить билет</a>
                </div>
            </div>
            <table id="tickets" class="table table-striped">
                <thead>
                    <tr>
                        <th data-name="event-name">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'event-name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'event-name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Мероприятие
                        </th>
                        <th data-name="category">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'category' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'category' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Категория билетов
                        </th>
                        <th data-name="sell_price">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'sell_price' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'sell_price' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Цена билетов
                        </th>
                        <th>Билетов всего</th>
                        <th>Билетов в бронировании</th>
                        <th>Билетов свободных</th>
                        <th data-name="description">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'description' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'description' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Описание
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="objects">
                    <tr>
                        <td>
                            <input type="text" class="form-control" name="event-name" value="{{ Request::get('event-name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="category" value="{{ Request::get('category') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="sell_price" value="{{ Request::get('sell_price') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control" name="description" value="{{ Request::get('description') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td></td>
                    </tr>
                    @foreach ($tickets as $ticket)
                    <tr>
                        <td>{{ $ticket->event->name }}</td>
                        <td>{{ $ticket->category}}</td>
                        <td>{{ $ticket->sell_price }}</td>
                        <td>{{ $seatsCount = $ticket->seats()->withTrashed()->count() }}</td>
                        <td>{{ $seatsBooked = $ticket->seats()->whereIn('transaction_id', $transactions)->count() }}</td>
                        <td>{{ $ticket->seats()->where('transaction_id', NULL)->count() }}</td>
                        <td>{{ $ticket->description }}</td>
                        <td>
                            <a href="{{ url('/stickler/ticket/' . $ticket->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            <a class="delete-entity" href="{{ url('/stickler/ticket/delete/' . $ticket->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                    @endforeach
                    @if(count($tickets) < 1)
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $tickets->links() }}</div>
                @if(Request::has('page'))
                <input type="hidden" name="page" value="{{ Request::get('page') }}">
                @endif
                <div class="col-md-6">
                    <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
        </form>
    </div>
    @include('includes.delete-entity')
@endsection