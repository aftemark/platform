@extends('layouts.app')

@section('title')Билеты @endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
        @include('includes.message-block')
        <form id="ticketForm" class="container" action="{{ url('/stickler/ticket/save') }}" method="POST">
            <div class="row">
                <div class="col-md-6">
                    @if($ticket->id)
                    <h3>Редактирование билетов</h3>
                    @else
                    <h3>Создание билетов</h3>
                    @endif
                </div>
                <div class="col-md-6 pull-right">
                    @if($allowSave)
                    <button type="submit" class="btn btn-success">Сохранить</button>
                    @endif
                    <a href="{{ url('/stickler/tickets') }}" class="btn btn-danger">Отменить</a>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    @if(isset($ticket->id))
                        <input type="hidden" name="id" value="{{ $ticket->id }}">
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <div class="form-group{{ $errors->has('event') ? ' has-error' : '' }}">
                            <label for="eventID">Мероприятие</label>
                            <select class="form-control" id="eventID" name="event_id">
                                <option value="">Выберите мероприятие</option>
                                @foreach ($events as $eventId => $eventName)
                                <option {{ $eventId == $ticket->event_id ? 'selected ' : '' }}value="{{ $eventId }}">{{ $eventName }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category">Категория билетов</label>
                            <input required id="category" name="category" type="text" class="form-control" value="{{ $ticket->category }}">
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group{{ $errors->has('cost_price') ? ' has-error' : '' }}">
                                <label for="cost_price">Цена себестоимости</label>
                                <input required id="cost_price" name="cost_price" type="text" class="form-control" value="{{ $ticket->cost_price }}">
                            </div>
                            <div class="col-md-6 form-group{{ $errors->has('sell_price') ? ' has-error' : '' }}">
                                <label for="sell_price">Цена продажи</label>
                                <input required id="sell_price" name="sell_price" type="text" class="form-control" value="{{ $ticket->sell_price }}">
                            </div>
                        </div>
                    </div>
                    <p>Комиссия, %</p>
                    <div class="row">
                        <div class="col-md-6 form-group{{ $errors->has('dealer_commission') ? ' has-error' : '' }}">
                            <label for="dealer_commission">Дилер</label>
                            <input required id="dealer_commission" name="dealer_commission" type="text" class="form-control" value="{{ $ticket->dealer_commission }}">
                        </div>
                        <div class="col-md-6 form-group{{ $errors->has('agent_commission') ? ' has-error' : '' }}">
                            <label for="agent_commission">Агент</label>
                            <input required id="agent_commission" name="agent_commission" type="text" class="form-control" value="{{ $ticket->agent_commission }}">
                        </div>
                    </div>
                    <p>Срок бронирования билетов</p>
                    <div class="row">
                        <div class="col-md-6 form-group{{ $errors->has('booking_days') ? ' has-error' : '' }}">
                            <label for="booking_days">Дней</label>
                            <input required id="booking_days" name="booking_days" type="text" class="form-control" value="{{ $ticket->booking_days or '0' }}">
                        </div>
                        <div class="col-md-6 form-group{{ $errors->has('booking_hours') ? ' has-error' : '' }}">
                            <label for="booking_hours">Часов</label>
                            <input required id="booking_hours" name="booking_hours" type="text" class="form-control" value="{{ $ticket->booking_hours or '0' }}">
                        </div>
                    </div>
                    <div class="row">
                        <p>Кто может продавать</p>
                        <div class="checkbox">
                            <label><input {{ $ticket->allow_seller ? 'checked ' : '' }}type="checkbox" name="allow_seller" value="true">Продавец</label>
                        </div>
                        <div class="checkbox">
                            <label><input {{ $ticket->allow_dealer ? 'checked ' : '' }}type="checkbox" name="allow_dealer" value="true">Дилер</label>
                        </div>
                        <div class="checkbox">
                            <label><input {{ $ticket->allow_agent ? 'checked ' : '' }}type="checkbox" name="allow_agent" value="true">Агент</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label><input {{ $ticket->custom_seats ? 'checked ' : '' }}id="switchType" type="checkbox" name="no_seats" value="true">Без мест</label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p id="seatsCount"{{ $ticket->custom_seats ? ' class=hidden' : '' }}>Привязано: {{ $ticket->seats->count() }} мест</p>
                            <div id="customCount" class="form-group{{ $ticket->custom_seats ? '' : ' hidden' }}">
                                <span>Привязать:</span>
                                <input type="text" name="custom_seats" value="{{ $ticket->custom_seats or '' }}">
                                <span> мест</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <textarea rows="7" name="description" id="description" class="form-control">{{ $ticket->description }}</textarea>
                    </div>
                </div>
            </div>
            <div id="canvasBlock" class="col-md-12{{ $ticket->custom_seats ? ' hidden' : '' }}">
                <p>Схема зала</p>
                <div style="width:804px;margin: 0 auto;border: 1px solid #aaa;" id="canvas-wrapper">
                    <canvas id="canvas" width="800" height="690"></canvas>
                </div>
            </div>
        </form>
@endsection

@section('script')
    <script src="{{ url('src/js/fabric/dist/fabric.min.js') }}"></script>
    <script src="{{ url('src/js/scheme.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        var csrf_token = '{{ Session::token() }}';
        (function () {
            @if($allowSave)
            $('#allow').select2();
            $('#ticketForm').submit(function() {
                let form = $(this);
                canvas.getActiveGroup().getObjects().forEach(function(object) {
                    form.append('<input type="hidden" name="seats[]" value="' + object.seatId + '"/>');
                });
            });
            $('#switchType').on('change', function() {
                    $('#seatsCount').toggleClass('hidden', this.checked);
                    $('#customCount').toggleClass('hidden', !this.checked);
                    @if($ticket->id)
                    $('#canvasBlock').toggleClass('hidden', this.checked);
                    @endif
            });
            $('#eventID').on('change', function() {
                let value = $(this).val();
                if (value)
                    fillHall({url: '{{ url('/stickler/ticket/hall') }}', id: '{{ $ticket->id }}', event_id: value, token: csrf_token});
            });
            @else
            $('#ticketForm').find('input, select, textarea').attr('disabled', true);
            @endif
        })();
        var canvas = new fabric.Canvas('canvas');
        const displayOnly = true;
        schemeInit();
        @if($ticket->event_id)
        fillHall({url: '{{ url('/stickler/ticket/hall') }}', id: '{{ $ticket->id }}', event_id: '{{ $ticket->event_id }}', token: csrf_token});
        @endif
    </script>
@endsection