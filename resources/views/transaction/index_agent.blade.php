@extends('layouts.app')

@section('title')Отчет по работе агентов @endsection

@section('content')
<div class="container">
    <form class="row">
        <div class="row">
            <h3>Отчет по работе агентов</h3>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th data-name="created_at">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Дата
                    </th>
                    <th data-name="user-name">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Агент
                    </th>
                    <th data-name="ticket-event-name">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-event-name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-event-name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Мероприятие
                    </th>
                    <th data-name="ticket-category">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-category' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-category' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Категория билета
                    </th>
                    <th>Количество билетов</th>
                    <th data-name="price">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'price' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'price' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Сумма сделки
                    </th>
                    <th data-name="user-percent">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-percent' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-percent' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Комиссия, %
                    </th>
                    <th data-name="">
                        Комиссия, руб.
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" class="form-control" name="created_at" value="{{ Request::get('created_at') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="user-name" value="{{ Request::get('user-name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="ticket-event-name" value="{{ Request::get('ticket-event-name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="ticket-category" value="{{ Request::get('ticket-category') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td></td>
                    <td>
                        <input type="text" class="form-control" name="price" value="{{ Request::get('price') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="user-percent" value="{{ Request::get('user-percent') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td></td>
                </tr>
                @foreach ($reports as $report)
                <tr>
                    <td>{{ $report->created_at }}</td>
                    <td>{{ $report->user->name }}</td>
                    <td>{{ $report->ticket->event->name }}</td>
                    <td>{{ $report->ticket->category  }}</td>
                    <td>{{ $report->tickets->count() }}</td>
                    <td>{{ $report->price or 'Бронь' }}</td>
                    <td>{{ $report->user->percent }}</td>
                    <td>{{ ($report->price * $report->user->percent) / 100 }}</td>
                </tr>
                @endforeach
                @if(count($reports) < 1)
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                @endif
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-6">{{ $reports->links() }}</div>
            @if(Request::has('page'))
            <input type="hidden" name="page" value="{{ Request::get('page') }}">
            @endif
            <div class="col-md-6">
                <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
            </div>
        </div>
    </form>
</div>
@endsection