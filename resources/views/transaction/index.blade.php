@extends('layouts.app')

@section('title')Список транзакций @endsection

@section('content')
<div class="container">
    <form class="row" onsubmit="tableSubmit(this.form)">
        <div class="row">
            <h3>Список транзакций</h3>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Дата и время
                    </th>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-created_at' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-created_at' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Дата регистрации
                    </th>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-user-name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-user-name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Пользователь
                    </th>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-main_role-display_name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-main_role-display_name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Тип пользователя
                    </th>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-event-name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-event-name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Мероприятие
                    </th>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-category' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'ticket-category' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Категория билета
                    </th>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'user-name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Кто купил
                    </th>
                    <th data-name="">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'price' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'price' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Сумма сделки
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" class="form-control" name="created_at" value="{{ Request::get('created_at') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="user-created_at" value="{{ Request::get('user-created_at') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="ticket-user-name" value="{{ Request::get('ticket-user-name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="user-main_role-display_name" value="{{ Request::get('user-main_role-display_name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="ticket-event-name" value="{{ Request::get('ticket-event-name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="ticket-category" value="{{ Request::get('ticket-category') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="user-name" value="{{ Request::get('user-name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="price" value="{{ Request::get('price') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                    </td>
                </tr>
                @foreach ($transactions as $transaction)
                <tr>
                    <td>{{ $transaction->created_at }}</td>
                    <td>{{ $transaction->user->created_at }}</td>
                    <td>{{ $transaction->ticket->user->name }}</td>
                    <td>{{ $transaction->user->main_role->display_name }}</td>
                    <td>{{ $transaction->ticket->event->name }}</td>
                    <td>{{ $transaction->ticket->category }}</td>
                    <td>{{ $transaction->user->name  }}</td>
                    <td>{{ $transaction->price }}</td>
                </tr>
                @endforeach
                @if(empty($transactions))
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                @endif
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-6">{{ $transactions->links() }}</div>
            @if(Request::has('page'))
            <input type="hidden" name="page" value="{{ Request::get('page') }}">
            @endif
            <div class="col-md-6">
                <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
            </div>
        </div>
    </form>
</div>
@endsection