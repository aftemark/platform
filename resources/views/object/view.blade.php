@extends('layouts.app')

@section('title')Объект @endsection

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Объект</h3>
                </div>
                <div class="col-md-6 pull-right">
                    <a class="btn btn-success" href="{{ route('events', ['object_id' => $object->id]) }}">Мероприятия</a>
                    <a href="{{ url('/objects') }}" class="btn btn-danger">Назад</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Название</label>
                        <input disabled type="text" class="form-control" value="{{ $object->name }}">
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Страна</label>
                            <input disabled type="text" class="form-control" value="{{ $object->country->name }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Город</label>
                            <input disabled type="text" class="form-control" value="{{ $object->city->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Адрес</label>
                        <input disabled type="text" class="form-control" value="{{ $object->address }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <textarea rows="7" class="form-control">{{ $object->description }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($object->links as $link)
                <div class="col-md-3">
                    <img src="{{ $link }}">
                </div>
                @endforeach
            </div>
        </div>
        <div class="modal fade" id="events" tabindex="-1" role="dialog" aria-labelledby="eventsLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="eventsLabel">Мероприятия на объекте</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ок</button>
                    </div>
                </div>
            </div>
        </div>
@endsection