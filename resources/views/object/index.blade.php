@extends('layouts.app')

@section('title')
    Список объектов
@endsection

@section('content')
    <div class="container">
        <form class="row">
            <div class="row">
                <div class="col-md-6"><h3>Список объектов</h3></div>
                @if(Auth::user()->hasRole(['stickler', 'seller']))
                <div class="col-md-6 pull-right"><a href="{{ url('/object/new') }}" class="btn btn-primary">Добавить объект</a></div>
                @endif
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th data-name="name">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Название
                        </th>
                        <th data-name="country_name">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'country_name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'country_name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Страна
                        </th>
                        <th data-name="city_name">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'city_name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'city_name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Город
                        </th>
                        <th data-name="address">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'address' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'address' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Адрес
                        </th>
                        <th data-name="description">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'description' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'description' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Описание
                        </th>
                        <th>Фото</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="objects">
                    <tr>
                        <td>
                            <input type="text" class="form-control" name="name" value="{{ Request::get('name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="country_name" value="{{ Request::get('country_name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="city_name" value="{{ Request::get('city_name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="address" value="{{ Request::get('address') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="description" value="{{ Request::get('description') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach ($objects as $object)
                    <tr>
                        <td>{{ $object->name }}</td>
                        <td>{{ $object->country->name }}</td>
                        <td>{{ $object->city->name }}</td>
                        <td>{{ $object->address }}</td>
                        <td>{{ $object->description }}</td>
                        @if(count($object->photos) > 0)
                            <td>
                                @foreach ($object->photos as $photo)
                                <a href="{{ $photo }}">{{ $photo }}</a>
                                @endforeach
                            </td>
                        @else
                            <td>-</td>
                        @endif
                        <td>
                            <a href="{{ url('/object/' . $object->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            @if(Auth::user()->hasRole(['stickler', 'seller']))
                            <a class="delete-entity" href="{{ url('/object/delete/' . $object->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @if(count($objects) < 1)
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $objects->links() }}</div>
                @if(Request::has('page'))
                <input type="hidden" name="page" value="{{ Request::get('page') }}">
                @endif
                <div class="col-md-6">
                    <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
        </form>
    </div>
    @if(Auth::user()->hasRole(['stickler', 'seller']))
    @include('includes.delete-entity')
    @endif
@endsection