<table class="table table-striped">
    <thead>
    <tr>
        <th>Название</th>
        <th>Время</th>
        <th>Объект</th>
        <th>Описание</th>
        <th>Фото</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($events as $event)
        <tr>
            @if(!empty($event->name))
            <td>{{ $event->name }}</td>
            @else
            <td>-</td>
            @endif
            @if(!empty($event->date))
            <td>{{ $event->date }}</td>
            @else
            <td>-</td>
            @endif
            @if(!empty($event->object->name))
            <td>{{ $event->object->name }}</td>
            @else
            <td>-</td>
            @endif
            @if(!empty($event->description))
            <td>{{ $event->description }}</td>
            @else
            <td>-</td>
            @endif
            @if(count($event->photos) > 0)
            <td>
            @foreach ($event->photos as $photo)
                <a href="{{ $photo }}">{{ $photo }}</a>
            @endforeach
            </td>
            @else
            <td>-</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>

<div class="col-md-6">{{ $events->links() }}</div>