@extends('layouts.app')

@section('title')
    Объект
@endsection

@section('content')
        <div class="container">
            @include('includes.message-block')
            <form action="{{ url('/object/save') }}" enctype="multipart/form-data" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Объект</h3>
                    </div>
                    <div class="col-md-6 pull-right">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <a href="{{ url('/objects') }}" class="btn btn-danger">Отменить</a>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        @if(isset($object["id"]))
                            <input type="hidden" name="id" value="{{ $object["id"] }}">
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input required name="name" type="text" class="form-control" placeholder="Название" value="{{ $object["name"] }}">
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                <label for="country">Страна</label>
                                <select class="form-control" id="country" name="country"></select>
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                <label for="city">Город</label>
                                <select class="form-control" id="city" name="city"></select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <input required name="address" type="text" class="form-control" placeholder="Адрес" value="{{ $object["address"] }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <textarea rows="7" name="description" class="form-control">{{ $object["description"] }}</textarea>
                        </div>
                    </div>
                    @if($object->id)
                    <div class="row">
                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <label for="fileupload">Добавить фотографии</label>
                            <input multiple class="form-control" id="fileupload" type="file" name="files[]">
                        </div>
                    </div>
                    @if(!empty($object->links))
                    <div class="row">
                        <div class="col-md-12 checkbox">
                            <label>
                                <input name="remove" type="checkbox" value="remove">
                                Удалить изображения
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($object->links as $link)
                        <div class="col-md-3">
                            <img src="{{ $link }}">
                        </div>
                        @endforeach
                    </div>
                    @endif
                    @endif
                </div>
                @if($object->id)
                <div class="row">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Список залов</h3>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-success" href="{{ url('/hall/new?object=' . $object->id ) }}">Добавить зал</a>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($object->halls as $hall)
                            <tr>
                                <td>{{ $hall->name }}</td>
                                <td>
                                    <a href="{{ url('/hall/' . $hall->id ) }}"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </form>
        </div>
@endsection

@section('script')
    @include('includes.city_country_script')
@endsection