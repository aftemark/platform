@extends('layouts.app')

@section('title')Агент @endsection

@section('content')
<section>
	<div class="container">
		@include('includes.message-block')
		<form action="{{ url('/agents') }}" method="POST">
			<div class="row">
				<div class="col-md-6">
					<h3>Пользователь</h3>
				</div>
				<div class="col-md-6 pull-right">
					<button type="submit" class="btn btn-success">Сохранить</button>
					<a href="{{ url('/agents') }}" class="btn btn-danger">Отменить</a>
					<input type="hidden" name="_token" value="{{ Session::token() }}">
					@if(isset($agent->id))
						<input type="hidden" name="id" value="{{ $agent->id }}">
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<input required name="name" type="text" class="form-control" placeholder="Имя" value="{{ $agent->name }}">
					</div>
					<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
						<input name="phone" type="text" class="form-control" placeholder="Телефон" value="{{ $agent->phone }}">
					</div>
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						@if(isset($agent->id))
							<input disabled type="text" class="form-control" placeholder="E-mail" value="{{ $agent->email }}">
						@else
							<input required type="email" name="email" class="form-control" placeholder="E-mail" value="{{ $agent->email }}">
						@endif
					</div>
					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<input name="password" type="password" class="form-control" placeholder="Пароль">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
						<label for="status">Статус агента</label>
						<select class="form-control" id="status" name="status">
							<option value="0">Не подтверждено</option>
							<option {{ $agent->status ? 'selected ' : '' }}value="1">Подтверждено</option>
						</select>
					</div>
					<div class="form-group{{ $errors->has('days') ? ' has-error' : '' }}">
						<input name="days" type="text" class="form-control" placeholder="Срок бронирования, дней" value="{{ $agent->days }}">
					</div>
					<div class="form-group{{ $errors->has('percent') ? ' has-error' : '' }}">
						<input name="percent" type="text" class="form-control" placeholder="Процент комиссии" value="{{ $agent->percent }}">
					</div>
					<div class="form-group{{ $errors->has('balance') ? ' has-error' : '' }}">
						<input name="balance" type="text" class="form-control" placeholder="Баланс" value="{{ $agent->balance }}">
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
@endsection