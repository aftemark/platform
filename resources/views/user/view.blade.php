@extends('layouts.app')

@section('title')Новый пользователь @endsection

@section('content')
	<section>
		<div class="container">
			@include('includes.message-block')
			<form action="{{ url('/user/save') }}" method="POST">
				<div class="row">
					<div class="col-md-6">
						<h3>Пользователь</h3>
					</div>
					<div class="col-md-6 pull-right">
						<button type="submit" class="btn btn-success">Сохранить</button>
						<a href="/stickler" class="btn btn-danger">Отменить</a>
						<input type="hidden" name="_token" value="{{ Session::token() }}">
						@if(isset($user["id"]))
							<input type="hidden" name="id" value="{{ $user["id"] }}">
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<input required name="name" type="text" class="form-control" placeholder="Имя" value="{{ $user["name"] }}">
						</div>
						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<input required name="phone" type="number" min="0" class="form-control" placeholder="Телефон" value="{{ $user["phone"] }}">
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							@if(isset($user["id"]))
								<input disabled type="email" class="form-control" placeholder="E-mail" value="{{ $user["email"] }}">
							@else
								<input required name="email" type="email" class="form-control" placeholder="E-mail" value="{{ $user["email"] }}">
							@endif
						</div>
						<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
							<label for="typeSelect">Тип пользователя</label>
							<select name="type" id="typeSelect" class="form-control">
								@foreach ($roles as $role)
									<option {{ $user->main_role_id == $role->id ? 'selected ' : '' }}value="{{ $role->name }}">{{ $role->display_name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<input name="password" type="password" class="form-control" placeholder="Пароль">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group{{ $errors->has('days') ? ' has-error' : '' }}">
							<input name="days" type="number" class="form-control{{ $errors->has('days') ? ' has-error' : '' }}" placeholder="Срок бронирования, дней" value="{{ $user["days"] }}">
						</div>
						<div class="form-group{{ $errors->has('percent') ? ' has-error' : '' }}">
							<input name="percent" type="number" class="form-control" placeholder="Процент комиссии" value="{{ $user["percent"] }}">
						</div>
						<div class="form-group{{ $errors->has('balance') ? ' has-error' : '' }}">
							<input name="balance" type="number" step="0.01" class="form-control" placeholder="Баланс" value="{{ $user["balance"] }}">
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
@endsection