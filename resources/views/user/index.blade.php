@extends('layouts.app')

@section('title')Список пользователей @endsection

@section('content')
    <div class="container">
        <form class="row" onsubmit="tableSubmit(this.form)">
            <div class="row">
                <div class="col-md-6"><h3>Список пользователей</h3></div>
                <div class="col-md-6 pull-right"><a href="{{ url('/user/new') }}" class="btn btn-primary">Добавить пользователя</a></div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Имя
                            <input type="text" class="form-control" name="name" value="{{ Request::get('name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'main_role_display_name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'main_role_display_name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Тип пользователя
                            <input type="text" class="form-control" name="main_role_display_name" value="{{ Request::get('main_role_display_name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Дата регистрации
                            <input type="text" class="form-control" name="created_at" value="{{ Request::get('created_at') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'balance' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'balance' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Баланс
                            <input type="text" class="form-control" name="balance" value="{{ Request::get('balance') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'percent' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'percent' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Процент комиссии
                            <input type="text" class="form-control" name="percent" value="{{ Request::get('percent') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'days' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'days' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Срок бронирования дней
                            <input type="text" class="form-control" name="days" value="{{ Request::get('days') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'phone' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'phone' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Телефон
                            <input type="text" class="form-control" name="phone" value="{{ Request::get('phone') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'email' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'email' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            E-mail
                            <input type="text" class="form-control" name="email" value="{{ Request::get('email') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="users">
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->main_role->display_name or '' }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->balance or '0.00' }}</td>
                        <td>{{ $user->percent }} %</td>
                        <td>{{ $user->days }}</td>
                        <td>{{ $user->phone or '-' }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a href="{{ url('/user/' . $user->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            <a class="delete-entity" href="{{ url('/user/delete/' . $user->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                    @endforeach
                    @if(!count($users))
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $users->links() }}</div>
                @if(Request::has('page'))
                <input type="hidden" name="page" value="{{ Request::get('page') }}">
                @endif
                <div class="col-md-6">
                    <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
        </form>
    </div>
    @include('includes.delete-entity')
@endsection