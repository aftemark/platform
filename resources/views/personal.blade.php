@extends('layouts.app')

@section('title')
    Личный кабинет
@endsection

@section('content')
    <section>
        <div class="container">

            <form action="{{ url('/personal') }}" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Пользователь</h3>
                    </div>
                    <div class="col-md-6 pull-right">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input required name="name" type="text" class="form-control" placeholder="Имя" value="{{ $user["name"] }}">
                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input required name="phone" type="text" class="form-control" placeholder="Телефон" value="{{ $user["phone"] }}">
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input required name="email" type="text" class="form-control" placeholder="E-mail" value="{{ $user["email"] }}">
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input name="password" type="password" class="form-control" placeholder="Новый пароль">
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input name="password_confirmation" type="password" class="form-control" placeholder="Новый пароль еще раз">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Текущий счет</p>
                        </div>
                        <div class="form-group">
                            <a data-toggle="modal" data-target="#withdraw" href="#withdraw" class="btn btn-default">Вывести средства</a>
                        </div>
                        @if($dealers)
                        <div class="form-group{{ $errors->has('dealer') ? ' has-error' : '' }}">
                            <label for="dealer">Дилер</label>
                            <select class="form-control" id="dealer" name="dealer">
                                <option value="">Убрать</option>
                                @foreach ($dealers as $ID => $name)
                                <option {{ $user->dealer_id == $ID ? 'selected ' : '' }}value="{{ $ID }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                </div>
            </form>
            @include('includes.message-block')
        </div>
    </section>
    <div class="modal fade" id="withdraw" tabindex="-1" role="dialog" aria-labelledby="withdrawLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="withdrawLabel">Вывод средств</h4>
                </div>
                <form action="{{ url('/withdraw') }}" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control" type="text" name="sum" placeholder="Сумма для вывода">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Отменить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @role('agent')
    <div class="modal fade" id="dealerConfirm" tabindex="-1" role="dialog" aria-labelledby="dealerConfirmLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="dealerConfirmLabel">Изменение дилера</h4>
                </div>
                <div class="modal-body">Для изменения дилера необходимо подтверждение дилера. Заявка оптравлена. Ожидайте.</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ок</button>
                </div>
            </div>
        </div>
    </div>
    @endrole
@endsection

@role('agent')
@section('script')
    <script>
        $("#dealer").on('change', function() {
            if ($("#dealer").val() != 'false')
                $('#dealerConfirm').modal('show');
        });
    </script>
@endsection
@endrole