@extends('layouts.app')

@section('title')Список агентов @endsection

@section('content')
    <div class="container">
        <form class="row">
            <div class="row">
                <div class="col-md-6"><h3>Список дилеров</h3></div>
                <div class="col-md-6 pull-right"><a href="{{ url('/dealers/new') }}" class="btn btn-primary">Добавить дилера</a></div>
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th data-name="name">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'name' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Имя
                    </th>
                    <th data-name="status">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'status' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'status' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Статус агента
                    </th>
                    <th data-name="created_at">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Дата регистрации
                    </th>
                    <th data-name="balance">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'balance' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'balance' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Баланс
                    </th>
                    <th data-name="percent">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'percent' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'percent' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Процент комиссии
                    </th>
                    <th data-name="days">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'days' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'days' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Срок бронирования дней
                    </th>
                    <th data-name="phone">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'phone' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'phone' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        Телефон
                    </th>
                    <th data-name="email">
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'email' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                        </div>
                        <div class="radio">
                            <label><input {{ Request::get('sort') == 'email' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                        </div>
                        E-mail
                    </th>
                    <th data-name="name"></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" class="form-control" name="name" value="{{ Request::get('name') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control" name="created_at" value="{{ Request::get('created_at') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="balance" value="{{ Request::get('balance') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="percent" value="{{ Request::get('percent') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="days" value="{{ Request::get('days') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="phone" value="{{ Request::get('phone') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="email" value="{{ Request::get('email') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td></td>
                    </tr>
                @foreach ($dealers as $dealer)
                    <tr>
                        @if(isset($dealer->name))
                            <td>{{ $dealer->name }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if($dealer->status)
                            <td>Подтверждено</td>
                        @else
                            <td>Не подтверждено</td>
                        @endif
                        @if(isset($dealer->created_at))
                            <td>{{ $dealer->created_at }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(isset($dealer->balance))
                            <td>{{ $dealer->balance }}</td>
                        @else
                            <td>0</td>
                        @endif
                        @if(isset($dealer->percent))
                            <td>{{ $dealer->percent }} %</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(isset($dealer->days))
                            <td>{{ $dealer->days }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(!empty($dealer->phone))
                            <td>{{ $dealer->phone }}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if(isset($dealer->email))
                            <td>{{ $dealer->email }}</td>
                        @else
                            <td>-</td>
                        @endif
                        <td>
                            <a href="{{ url('/dealers/' . $dealer->id ) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            <a class="delete-entity" href="{{ url('/dealers/delete/' . $dealer->id ) }}"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $dealers->links() }}</div>
                <div class="col-md-6">
                    <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
        </form>
    </div>
    @include('includes.delete-entity')
@endsection