@extends('layouts.app')

@section('title')Дилер @endsection

@section('content')
<section>
	<div class="container">
		@include('includes.message-block')
		<form action="{{ url('/dealers') }}" method="POST">
			<div class="row">
				<div class="col-md-6">
					<h3>Дилер</h3>
				</div>
				<div class="col-md-6 pull-right">
					<button type="submit" class="btn btn-success">Сохранить</button>
					<a href="{{ url('/dealers') }}" class="btn btn-danger">Отменить</a>
					<input type="hidden" name="_token" value="{{ Session::token() }}">
					@if(isset($dealer->id))
						<input type="hidden" name="id" value="{{ $dealer->id }}">
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-6{{ $errors->has('name') ? ' has-error' : '' }}">
					<div class="form-group">
						<input required name="name" type="text" class="form-control" placeholder="Имя" value="{{ $dealer->name }}">
					</div>
					<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
						<input name="phone" type="text" class="form-control" placeholder="Телефон" value="{{ $dealer->phone }}">
					</div>
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						@if(isset($dealer->id))
							<input disabled type="text" class="form-control" placeholder="E-mail" value="{{ $dealer->email }}">
						@else
							<input required name="email" type="email" class="form-control" placeholder="E-mail" value="{{ $dealer->email }}">
						@endif
					</div>
					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<input name="password" type="password" class="form-control" placeholder="Пароль">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
						<label for="status">Статус дилера</label>
						<select class="form-control" id="status" name="status">
							<option value="0">Не подтверждено</option>
							<option {{ $dealer->status ? 'selected ' : '' }}value="1">Подтверждено</option>
						</select>
					</div>
					<div class="form-group{{ $errors->has('percent') ? ' has-error' : '' }}">
						<input name="percent" type="text" class="form-control" placeholder="Процент комиссии" value="{{ $dealer->percent }}">
					</div>
					<div class="form-group{{ $errors->has('balance') ? ' has-error' : '' }}">
						<input name="balance" type="text" class="form-control" placeholder="Баланс" value="{{ $dealer->balance }}">
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
@endsection