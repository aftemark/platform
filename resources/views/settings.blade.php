@extends('layouts.app')

@section('title')
    Личный кабинет
@endsection

@section('content')
    <section>
        <div class="container">
            <form action="{{{ url('/stickler/settings') }}}" enctype="multipart/form-data" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Настройки</h3>
                    </div>
                    <div class="col-md-6 pull-right">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('percent') ? ' has-error' : '' }}">
                            <input required name="percent" type="text" class="form-control" placeholder="Процент комиссии для дилера по умолчанию" value="{{ $settings["percent"] }}">
                        </div>
                        <div class="form-group{{ $errors->has('days') ? ' has-error' : '' }}">
                            <input name="days" type="text" class="form-control" placeholder="Период бронирования средств, после мероприятия, дней" value="{{ $settings["days"] }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Текущий счет: {{ $settings["balance"] }} руб.</p>
                        </div>
                        <div class="form-group">
                            <a href="{{ url('/withdraw') }}" class="btn btn-default">Вывести средства</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file">Пользовательское соглашение</label>
                            <input id="file" type="file" class="form-control" name="file">
                        </div>
                    </div>
                </div>
            </form>
            @include('includes.message-block')
        </div>
    </section>
@endsection