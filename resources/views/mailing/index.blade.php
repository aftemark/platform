@extends('layouts.app')

@section('title')Рассылки @endsection

@section('content')
    <div class="container">
        <form class="row text-center" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Получатели</label>
                <select name="receiver" class="form-control">
                    <option value="">Выберите получателей</option>
                    <option>Дилеры</option>
                    <option>Агенты</option>
                    <option>Продавцы</option>
                    <option>Покупатели</option>
                </select>
            </div>
            <div class="form-group">
                <label>Тема сообщения</label>
                <input type="text" name="subject" class="form-control">
            </div>
            <div class="form-group">
                <label>Текст сообщения</label>
                <textarea name="body" class="form-control"></textarea>
            </div>
            <button class="btn btn-default">Отправить</button>
        </form>
        <hr>
        <form class="row">
            <div class="row">
                <div class="col-md-6">
                    <h3>Рассылки</h3>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th data-name="created_at">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'created_at' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Дата
                        </th>
                        <th data-name="receiver">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'receiver' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'receiver' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Получатели
                        </th>
                        <th data-name="body">
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'body' && Request::get('order') == 'asc' ? 'checked ' : '' }}type="radio" name="order" value="asc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-up"></span></label>
                            </div>
                            <div class="radio">
                                <label><input {{ Request::get('sort') == 'body' && Request::get('order') == 'desc' ? 'checked ' : '' }}type="radio" name="order" value="desc" onchange="tableSubmit(this.form)"><span class="glyphicon glyphicon-chevron-down"></span></label>
                            </div>
                            Текст
                        </th>
                    </tr>
                </thead>
                <tbody id="objects">
                    <tr>
                        <td>
                            <input type="text" class="form-control" name="created_at" value="{{ Request::get('created_at') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="receiver" value="{{ Request::get('receiver') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="body" value="{{ Request::get('body') }}" onkeydown="event.keyCode == 13 ? tableSubmit(this.form) : false">
                        </td>
                    </tr>
                    @foreach ($mailings as $mailing)
                    <tr>
                        <td>{{ $mailing->created_at }}</td>
                        <td>{{ $mailing->receiver }}</td>
                        <td>{!! $mailing->body !!}</td>
                    </tr>
                    @endforeach
                    @if(count($mailings) < 1)
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">{{ $mailings->links() }}</div>
                @if(Request::has('page'))
                <input type="hidden" name="page" value="{{ Request::get('page') }}">
                @endif
                <div class="col-md-6">
                    <a href="{{ url('/pagination?num=10') }}" class="btn btn-default">10</a>
                    <a href="{{ url('/pagination?num=50') }}" class="btn btn-default">50</a>
                    <a href="{{ url('/pagination?num=100') }}" class="btn btn-default">100</a>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('body');
    </script>
@endsection