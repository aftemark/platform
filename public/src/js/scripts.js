function tableSubmit(form) {
    var $form = $(form),
        $radio = $form.find('input:checked');

    $form.find('input[type=text]').each(function () {
        var $this =  $(this);
        if (!$this.val())
            $this.attr('disabled', true);
    });

    if ($radio.length) {
        $form.append('<input type="hidden" name="sort" value="' + $radio.closest('th').attr('data-name') + '"/>');
    }

    form.submit();
}

function loadElements(selector) {
    let element = $(selector);
    element.on('change', function () {
        let target = $(element.attr('data-id')),
            value = element.val();

        if (value == 'false') {
            target.empty();
            return;
        }

        $.post(element.attr('data-url'), {
            'id': value,
            '_token': element.closest('form').find('[name=_token]').val()
        }, function (response) {
            target.empty();
            $.each(response, function (id, name) {
                target.append('<option value="' + id + '">' + name + '</option>')
            });
        });
    });
}

function showModelRelations(model) {
    let $element = $(model),
    target = $($element.attr('data-target'));
    $.ajax({
        type: "GET",
        url: $element.attr('href'),
        dataType: "html",
        success: function (data) {
            target.find('.modal-body').empty().append(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            target.find('.modal-body').empty().append(thrownError);
        }
    });
}


(function () {
    $('.delete-entity').on('click', function(e) {
        let $element = $(this),
        $modal = $('#deleteEntityModal');
        $.ajax({
            type: "GET",
            url: $element.attr('href'),
            success: function() {
                window.location.reload();
            },
            error: function (error) {
                //$modal.find('.modal-body').empty().append(error.getResponseHeader('errorText'));
                $modal.modal('show');
            }
        });
        e.preventDefault();
    })
})();