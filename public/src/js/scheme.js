const rectSize = 23,
    rectColor = '#5C7A99',
    occupiedColor = '#FF7676';

function schemeInit() {
    canvas.on('object:selected', function () {
        let group = canvas.getActiveObject();
        if (group && group.get('type') == 'group') {
            let $seatInfo = $('#seatInfo'),
                inputs = $seatInfo.find('input');
            $seatInfo.removeClass('hidden');
            $seatInfo.find('[name=row]').val(group.row);
            $seatInfo.find('[name=num]').val(group.num);
            inputs.unbind();
            inputs.on('change', function () {
                if ($(this).val() < 0)
                    $(this).val(Math.abs($(this).val()));
                $.each(group.getObjects(), function (_, object) {
                    if (object.get('type') == 'text') {
                        let row = $seatInfo.find('[name=row]').val(),
                            num = $seatInfo.find('[name=num]').val();
                        object.setText(row + '-' + num);
                        group.num = num;
                        group.row = row;

                        canvas.renderAll();
                    }
                });
            });
        } else
            $('#seatInfo').addClass('hidden');
    });

    canvas.on('before:selection:cleared', function () {
        $('#seatInfo').addClass('hidden');
    });

    canvas.on('selection:created', function (e) {
        e.target.set({
            hasControls: false
        });
    });

    fabric.Object.prototype.toObject = (function (toObject) {
        return function (properties) {
            return fabric.util.object.extend(toObject.call(this, properties), {
                seatId: this.seatId,
                num: this.num,
                row: this.row
            });
        };
    })(fabric.Object.prototype.toObject);
}

function addText(text, left, top, scaleX, scaleY) {
    let iText = new fabric.IText(!text ? 'Изменить двойным нажатием.' : text, {
        fontSize: 20,
        left: !left ? canvas.getWidth() / 2 : left,
        top: !top ? canvas.getHeight() / 2 : top,
        borderColor: occupiedColor,
        //fontFamily: 'helvetica',
        originX: 'center',
        originY: 'center',
        scaleX: !scaleX ? 1 : scaleX,
        scaleY: !scaleY ? 1 : scaleY,
        hasRotatingPoint: false,
        centerTransform: true,
        hasControls: !displayOnly,
        lockMovementX: displayOnly,
        lockMovementY: displayOnly
    });

    canvas.add(iText);
    iText.selectable = !displayOnly;
    return iText;
}

function addRect(left, top, row, num, seatId, occupied = false) {
    let rect = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: !occupied ? rectColor : occupiedColor,
            width: rectSize,
            height: rectSize
        }),
        text = new fabric.Text(row + '-' + num, {
            fontSize: rectSize / 3,
            fill: '#FFFFFF',
            originX: 'center',
            originY: 'center'
        }),
        group = new fabric.Group([rect, text], {
            originX: 'center',
            originY: 'center',
            hasControls: false,
            borderColor: occupiedColor,
            left: left,
            top: top,
            lockMovementX: displayOnly,
            lockMovementY: displayOnly
        });

    group.seatId = seatId;
    group.num = num;
    group.row = row;

    canvas.add(group);
    group.selectable = !occupied;
    return group;
}

function removeObjects() {
    let activeObject = canvas.getActiveObject(),
        activeGroup = canvas.getActiveGroup();

    if (activeGroup) {
        var objectsInGroup = activeGroup.getObjects();
        canvas.discardActiveGroup();
        objectsInGroup.forEach(function(object) {
            canvas.remove(object);
        });
    } else if (activeObject) {
        canvas.remove(activeObject);
    }
}

function fillHall(parameters) {
    canvas.clear();
    $.post(parameters.url, {
        'id': parameters.id,
        'event_id': parameters.event_id,
        '_token': parameters.token
    }).success(function (data) {
        let selected = [];
        $.each(data.seats, function (_, seat) {
            let group = addRect(seat.left, seat.top, seat.row, seat.seat, seat.id, seat.status == 'occupied');
            if (seat.status == 'selected') {
                group.set('active', true);
                selected.push(group);
            }
        });
        $.each(data.texts, function (_, text) {
            addText(text.body, text.left, text.top, text.scale_x, text.scale_y);
        });

        let selectGroup = new fabric.Group(selected, {
            originX: 'center',
            originY: 'center',
            hasControls: false
        });
        canvas._activeObject = null;
        canvas.setActiveGroup(selectGroup.setCoords()).renderAll();
    });
}